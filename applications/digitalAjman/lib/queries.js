'use strict';

// const { getStatus, fileTypes, getOrgTypes } = require('./common');
let sql = require('mssql');
const moment = require('moment');


function allllll(data,conn){
    let {
        interactionId,
        orgId,
        serviceId,
        branchId,
        type,
        nationality,
        gender,
        age,
        detectedLanguage,
        currentStatus,
        submissionTime,
        providedText,
        customerProvidedSentiment,
        isVIPPerson,
        VIPPersonKeyText,
        startTimeTextAnalytics,
        endTimeTextAnalytics,
        startTimeLanguageConversion,
        endTimeLanguageConversion,
        convertedText,
        detectedMajorSentimentText,
        detectedMajorSentimentTextScore,
        keyphrases,
        NER,
        textAnalyticsResponse,
        startTimeVideoIndexer,
        endTimeVideoIndexer,
        videoDurationInSeconds,
        transcriptText,
        detectedMajorSentimentVideo,
        detectedMajorSentimentVideoScore,
        detectedMajorEmotionVideo,
        detectedMajorEmotionVideoScore,
        keywords,
        labels,
        scenes,
        brands,
        videoStatistics,
        audioEffects,
        visualContentModeration_video_adultScore,
        visualContentModeration_video_racyScore,
        visualContentModeration_text_adultScore,
        visualContentModeration_text_racyScore,
        topics,
        sourceLanguage,
        language,
        videoindexerUploadResponse,
        statusCompleted,
        detectedMajorEmotionFace,
        detectedMajorEmotionFaceScore,
        detectedMajorSentimentFace,
        detectedMajorSentimentFaceScore,
        opinionsAndAspects,
        garbageEntity,
        conclusiveSentiment,
        conclusiveSentimentScore
    } = data;

    conn.input('interactionId', sql.VarChar, interactionId)
    conn.input('orgId', sql.VarChar, orgId)
    conn.input('serviceId', sql.VarChar, serviceId)
    conn.input('branchId', sql.VarChar, branchId)
    conn.input('type', sql.VarChar, type)
    conn.input('nationality', sql.VarChar, nationality)
    conn.input('gender', sql.VarChar, gender)
    conn.input('age', sql.Float, age)
    conn.input('detectedLanguage', sql.VarChar, detectedLanguage)
    conn.input('currentStatus', sql.VarChar, currentStatus)
    conn.input('submissionTime', sql.DateTime, submissionTime)
    conn.input('providedText', sql.NVarChar, providedText)
    conn.input('customerProvidedSentiment', sql.VarChar, customerProvidedSentiment)
    conn.input('isVIPPerson', sql.Bit, isVIPPerson)
    conn.input('VIPPersonKeyText', sql.NVarChar, VIPPersonKeyText)
    conn.input('startTimeTextAnalytics', sql.DateTime, startTimeTextAnalytics)
    conn.input('endTimeTextAnalytics', sql.DateTime, endTimeTextAnalytics)
    conn.input('startTimeLanguageConversion', sql.DateTime, startTimeLanguageConversion)
    conn.input('endTimeLanguageConversion', sql.DateTime, endTimeLanguageConversion)
    conn.input('convertedText', sql.NVarChar, convertedText)
    conn.input('detectedMajorSentimentText', sql.NVarChar, detectedMajorSentimentText)
    conn.input('detectedMajorSentimentTextScore', sql.Float, detectedMajorSentimentTextScore)
    conn.input('keyphrases', sql.NVarChar, keyphrases)
    conn.input('NER', sql.NVarChar, NER)
    conn.input('textAnalyticsResponse', sql.NVarChar, textAnalyticsResponse)
    conn.input('startTimeVideoIndexer', sql.DateTime, startTimeVideoIndexer)
    conn.input('endTimeVideoIndexer', sql.DateTime, endTimeVideoIndexer)
    conn.input('videoDurationInSeconds', sql.Float, videoDurationInSeconds)
    conn.input('transcriptText', sql.NVarChar, transcriptText)
    conn.input('detectedPersonID', sql.NVarChar, detectedPersonID)
    conn.input('detectedMajorSentimentVideo', sql.NVarChar, detectedMajorSentimentVideo)
    conn.input('detectedMajorSentimentVideoScore', sql.Float, detectedMajorSentimentVideoScore)
    conn.input('detectedMajorEmotionVideo', sql.NVarChar, detectedMajorEmotionVideo)
    conn.input('detectedMajorEmotionVideoScore', sql.Float, detectedMajorEmotionVideoScore)
    conn.input('keywords', sql.NVarChar, keywords)
    conn.input('labels', sql.NVarChar, labels)
    conn.input('scenes', sql.NVarChar, scenes)
    conn.input('brands', sql.NVarChar, brands)
    conn.input('videoStatistics', sql.NVarChar, videoStatistics)
    conn.input('audioEffects', sql.NVarChar, audioEffects)
    conn.input('visualContentModeration_video_adultScore', sql.NVarChar, visualContentModeration_video_adultScore)
    conn.input('visualContentModeration_video_racyScore', sql.NVarChar, visualContentModeration_video_racyScore)
    conn.input('visualContentModeration_text_adultScore', sql.NVarChar, visualContentModeration_text_adultScore)
    conn.input('visualContentModeration_text_racyScore', sql.NVarChar, visualContentModeration_text_racyScore)
    conn.input('topics', sql.NVarChar, topics)
    conn.input('sourceLanguage', sql.VarChar, sourceLanguage)
    conn.input('language', sql.VarChar, language)
    conn.input('videoindexerUploadResponse', sql.NVarChar, videoindexerUploadResponse)
    conn.input('statusCompleted', sql.NVarChar, statusCompleted)
    conn.input('detectedMajorEmotionFace', sql.NVarChar, detectedMajorEmotionFace)
    conn.input('detectedMajorEmotionFaceScore', sql.Float, detectedMajorEmotionFaceScore)
    conn.input('opinionsAndAspects', sql.NVarChar, opinionsAndAspects)
    conn.input('garbageEntity', sql.Bit, garbageEntity)
    conn.input('conclusiveSentiment', sql.VarChar, conclusiveSentiment)
    conn.input('conclusiveSentimentScore', sql.Float, conclusiveSentimentScore)
}


function insertInteraction(data,conn){
    let {
        interactionId,
        orgId,
        type,
        detectedLanguage,
        currentStatus,
        submissionTime,
        providedText,
        startTimeTextAnalytics,
        videoindexerUploadResponse,
        statusCompleted,
        garbageEntity,
        filePath,
        startTimeLanguageConversion,
        endTimeLanguageConversion,
        convertedText,
    } = data;

    console.log(data,"data----------------->");

    let query;

    //video_audio insert case
    if(currentStatus=="RECEIVED" && (type=="A" || type=="V")){
        conn.input('interactionId', sql.VarChar, interactionId);
        conn.input('orgId', sql.VarChar, orgId);
        conn.input('type', sql.VarChar, type);
        conn.input('currentStatus', sql.VarChar, currentStatus);
        conn.input('submissionTime', sql.DateTime, submissionTime);
        conn.input('videoindexerUploadResponse', sql.NVarChar, JSON.stringify(videoindexerUploadResponse));
        conn.input('filePath', sql.NVarChar, filePath);


        // query=`INSERT INTO digital_ajman.dbo.interaction 
        // (
        //     interactionId,
        //     type,
        //     orgId,
        //     currentStatus,
        //     submissionTime,
        //     videoindexerUploadResponse,
        //     filePath
        // ) 
        // VALUES (
        //     @interactionId,
        //     @type,
        //     @orgId,
        //     @currentStatus,
        //     @submissionTime,
        //     @videoindexerUploadResponse,
        //     @filePath
        // )`

        query=`UPDATE digital_ajman.dbo.interaction 
        SET
            type=@type,
            orgId=@orgId,
            currentStatus=@currentStatus,
            submissionTime=@submissionTime,
            videoindexerUploadResponse=@videoindexerUploadResponse,
            filePath=@filePath
        WHERE
            interactionId=@interactionId`
    }

    if(currentStatus=="RECEIVED" && type=="T" ){
        conn.input('interactionId01', sql.VarChar, interactionId);
        conn.input('orgId01', sql.VarChar, orgId);
        conn.input('type01', sql.VarChar, type);
        conn.input('garbageEntity01', sql.Bit, garbageEntity);
        conn.input('currentStatus01', sql.VarChar, currentStatus);
        conn.input('submissionTime01', sql.DateTime, submissionTime);
        conn.input('providedText01', sql.NVarChar, providedText);


        query=`UPDATE digital_ajman.dbo.interaction 
        SET
            type=@type01,
            garbageEntity=@garbageEntity01,
            orgId=@orgId01,
            currentStatus=@currentStatus01,
            submissionTime=@submissionTime01,
            providedText=@providedText01
        WHERE
            interactionId=@interactionId01`
    }

    //test input insert case
    if(currentStatus=="ANALYZING KEYPHRASES" && type=="T" ){
        conn.input('interactionId', sql.VarChar, interactionId);
        conn.input('orgId', sql.VarChar, orgId);
        conn.input('type', sql.VarChar, type);
        conn.input('garbageEntity', sql.Bit, garbageEntity);
        conn.input('detectedLanguage', sql.VarChar, detectedLanguage);
        conn.input('currentStatus', sql.VarChar, currentStatus);
        conn.input('submissionTime', sql.DateTime, submissionTime);
        conn.input('providedText', sql.NVarChar, providedText);
        conn.input('startTimeTextAnalytics', sql.DateTime, startTimeTextAnalytics);
        conn.input('statusCompleted', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('startTimeLanguageConversion', sql.DateTime, startTimeLanguageConversion)
        conn.input('endTimeLanguageConversion', sql.DateTime, endTimeLanguageConversion)
        conn.input('convertedText', sql.NVarChar, JSON.stringify(convertedText))

        // query=`INSERT INTO digital_ajman.dbo.interaction 
        // (
        //     interactionId,
        //     type,
        //     garbageEntity,
        //     orgId,
        //     detectedLanguage,
        //     currentStatus,
        //     submissionTime,
        //     providedText,
        //     startTimeTextAnalytics,
        //     statusCompleted
        // )
        // VALUES (
        //     @interactionId,
        //     @type,
        //     @garbageEntity,
        //     @orgId,
        //     @detectedLanguage,
        //     @currentStatus,
        //     @submissionTime,
        //     @providedText,
        //     @startTimeTextAnalytics,
        //     @statusCompleted
        // )`

        query=`UPDATE digital_ajman.dbo.interaction 
        SET
            type=@type,
            garbageEntity=@garbageEntity,
            orgId=@orgId,
            detectedLanguage=@detectedLanguage,
            currentStatus=@currentStatus,
            submissionTime=@submissionTime,
            providedText=@providedText,
            startTimeTextAnalytics=@startTimeTextAnalytics,
            statusCompleted=@statusCompleted,
            startTimeLanguageConversion=@startTimeLanguageConversion,
            endTimeLanguageConversion=@endTimeLanguageConversion,
            convertedText=@convertedText
        WHERE
            interactionId=@interactionId`
    }

    console.log(query,"query----------------->");

    return [query,conn];

}

function updateInteraction(data,conn){
    let {
        interactionId,
        orgId,
        serviceId,
        branchId,
        type,
        nationality,
        gender,
        age,
        detectedLanguage,
        currentStatus,
        submissionTime,
        providedText,
        customerProvidedSentiment,
        isVIPPerson,
        VIPPersonKeyText,
        startTimeTextAnalytics,
        endTimeTextAnalytics,
        startTimeLanguageConversion,
        endTimeLanguageConversion,
        convertedText,
        detectedMajorSentimentText,
        detectedMajorSentimentTextScore,
        keyphrases,
        NER,
        textAnalyticsResponse,
        startTimeVideoIndexer,
        endTimeVideoIndexer,
        videoDurationInSeconds,
        transcriptText,
        detectedPersonID,
        entityName,
        detectedMajorSentimentVideo,
        detectedMajorSentimentVideoScore,
        detectedMajorEmotionVideo,
        detectedMajorEmotionVideoScore,
        detectedOrgId,
        keywords,
        labels,
        scenes,
        brands,
        videoStatistics,
        audioEffects,
        visualContentModeration_video_adultScore,
        visualContentModeration_video_racyScore,
        visualContentModeration_text_adultScore,
        visualContentModeration_text_racyScore,
        topics,
        sourceLanguage,
        language,
        videoindexerUploadResponse,
        statusCompleted,
        detectedMajorEmotionFace,
        detectedMajorEmotionFaceScore,
        detectedMajorSentimentFace,
        detectedMajorSentimentFaceScore,
        opinionsAndAspects,
        garbageEntity,
        conclusiveSentiment,
        conclusiveSentimentScore
    } = data;

    console.log(data,"data-------------------------->");

    let query;

    if(currentStatus=="ANALYZING VIDEO" || currentStatus=="ANALYZING AUDIO"){
        conn.input('interactionId1', sql.VarChar, interactionId);
        conn.input('statusCompleted1', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('currentStatus1', sql.VarChar, currentStatus);
        conn.input('startTimeVideoIndexer1', sql.DateTime, startTimeVideoIndexer);

        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        currentStatus=@currentStatus1,
        statusCompleted=@statusCompleted1,
        startTimeVideoIndexer=@startTimeVideoIndexer1
        WHERE 
        interactionId=@interactionId1`
    }

    if(currentStatus=="RECOGNIZING ENTITIES"){
        conn.input('interactionId2', sql.VarChar, interactionId);
        conn.input('statusCompleted2', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('currentStatus2', sql.VarChar, currentStatus);
        conn.input('keyphrases2', sql.NVarChar, JSON.stringify(keyphrases))

        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        currentStatus=@currentStatus2,
        statusCompleted=@statusCompleted2,
        keyphrases=@keyphrases2
        WHERE 
        interactionId=@interactionId2`
    }

    if(currentStatus=="ANALYZING TEXT"){
        conn.input('interactionId3', sql.VarChar, interactionId);
        conn.input('statusCompleted3', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('currentStatus3', sql.VarChar, currentStatus);
        conn.input('NER3', sql.NVarChar, JSON.stringify(NER))

        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        currentStatus=@currentStatus3,
        statusCompleted=@statusCompleted3,
        NER=@NER3
        WHERE 
        interactionId=@interactionId3`
    }

    if(currentStatus=="COMPLETED" && garbageEntity==0){
        conn.input('interactionId4', sql.VarChar, interactionId);
        conn.input('statusCompleted4', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('currentStatus4', sql.VarChar, currentStatus);
        conn.input('endTimeTextAnalytics4', sql.DateTime, endTimeTextAnalytics);
        conn.input('detectedMajorSentimentText4', sql.NVarChar, detectedMajorSentimentText);
        conn.input('detectedMajorSentimentTextScore4', sql.Float, detectedMajorSentimentTextScore*100);
        conn.input('textAnalyticsResponse4', sql.NVarChar, JSON.stringify(textAnalyticsResponse));
        conn.input('opinionsAndAspects4', sql.NVarChar, JSON.stringify(opinionsAndAspects));
        conn.input('detectedOrgId4', sql.NVarChar, detectedOrgId);
        conn.input('entityName4', sql.NVarChar, entityName)
        conn.input('conclusiveSentiment4', sql.VarChar, conclusiveSentiment)
        conn.input('conclusiveSentimentScore4', sql.Float, conclusiveSentimentScore?conclusiveSentimentScore*100:0.5*100)

        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        currentStatus=@currentStatus4,
        statusCompleted=@statusCompleted4,
        endTimeTextAnalytics=@endTimeTextAnalytics4,
        detectedMajorSentimentText=@detectedMajorSentimentText4,
        detectedMajorSentimentTextScore=@detectedMajorSentimentTextScore4,
        textAnalyticsResponse=@textAnalyticsResponse4,
        detectedOrgId=@detectedOrgId4,
        entityName=@entityName4,
        opinionsAndAspects=@opinionsAndAspects4,
        conclusiveSentiment=@conclusiveSentiment4,
        conclusiveSentimentScore=@conclusiveSentimentScore4
        WHERE 
        interactionId=@interactionId4`
    }
    
    if(currentStatus=="DETECTING LANGUAGE"){
        conn.input('interactionId5', sql.VarChar, interactionId);
        conn.input('statusCompleted5', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('currentStatus5', sql.VarChar, currentStatus);
        conn.input('garbageEntity5', sql.Bit, garbageEntity);
        conn.input('startTimeTextAnalytics5', sql.DateTime, startTimeTextAnalytics);
        conn.input('endTimeVideoIndexer5', sql.DateTime, endTimeVideoIndexer);
        conn.input('videoDurationInSeconds5', sql.Float, videoDurationInSeconds);
        conn.input('detectedMajorSentimentVideo5', sql.NVarChar, detectedMajorSentimentVideo);
        conn.input('detectedMajorSentimentVideoScore5', sql.Float, detectedMajorSentimentVideoScore*100);
        conn.input('detectedMajorEmotionVideo5', sql.NVarChar, detectedMajorEmotionVideo);
        conn.input('detectedMajorEmotionVideoScore5', sql.Float, detectedMajorEmotionVideoScore*100);
        conn.input('transcriptText5', sql.NVarChar, JSON.stringify(transcriptText));
        conn.input('keywords5', sql.NVarChar, JSON.stringify(keywords));
        conn.input('labels5', sql.NVarChar, JSON.stringify(labels));
        conn.input('scenes5', sql.NVarChar, JSON.stringify(scenes));
        conn.input('brands5', sql.NVarChar, JSON.stringify(brands));
        conn.input('topics5', sql.NVarChar, JSON.stringify(topics));
        conn.input('videoStatistics5', sql.NVarChar, JSON.stringify(videoStatistics));
        conn.input('audioEffects5', sql.NVarChar, JSON.stringify(audioEffects));
        conn.input('sourceLanguage5', sql.VarChar, sourceLanguage);
        conn.input('language5', sql.VarChar, language);
        conn.input('detectedOrgId5', sql.NVarChar, detectedOrgId);
        
        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        currentStatus=@currentStatus5,
        statusCompleted=@statusCompleted5,
        garbageEntity=@garbageEntity5,
        startTimeTextAnalytics=@startTimeTextAnalytics5,
        endTimeVideoIndexer=@endTimeVideoIndexer5,
        videoDurationInSeconds=@videoDurationInSeconds5,
        detectedMajorSentimentVideo=@detectedMajorSentimentVideo5,
        detectedMajorSentimentVideoScore=@detectedMajorSentimentVideoScore5,
        detectedMajorEmotionVideo=@detectedMajorEmotionVideo5,
        detectedMajorEmotionVideoScore=@detectedMajorEmotionVideoScore5,
        transcriptText=@transcriptText5,
        keywords=@keywords5,
        labels=@labels5,
        brands=@brands5,
        scenes=@scenes5,
        topics=@topics5,
        audioEffects=@audioEffects5,
        detectedOrgId=@detectedOrgId5,
        videoStatistics=@videoStatistics5,
        language=@language5,
        sourceLanguage=@sourceLanguage5
        WHERE 
        interactionId=@interactionId5`
    }

    if(currentStatus=="ANALYZING KEYPHRASES"){
        conn.input('interactionId6', sql.VarChar, interactionId);
        conn.input('detectedLanguage6', sql.VarChar, detectedLanguage);
        conn.input('currentStatus6', sql.VarChar, currentStatus);
        conn.input('statusCompleted6', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('convertedText6', sql.NVarChar, JSON.stringify(convertedText));
        conn.input('startTimeLanguageConversion6', sql.DateTime, startTimeLanguageConversion)
        conn.input('endTimeLanguageConversion6', sql.DateTime, endTimeLanguageConversion)

        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        interactionId=@interactionId6,
        detectedLanguage=@detectedLanguage6,
        currentStatus=@currentStatus6,
        convertedText=@convertedText6,
        startTimeLanguageConversion=@startTimeLanguageConversion6,
        endTimeLanguageConversion=@endTimeLanguageConversion6,
        statusCompleted= @statusCompleted6
        WHERE 
        interactionId=@interactionId6`
    }

    if(currentStatus=="COMPLETED" && garbageEntity==1){
        conn.input('interactionId7', sql.VarChar, interactionId);
        conn.input('statusCompleted7', sql.NVarChar, JSON.stringify(statusCompleted));
        conn.input('currentStatus7', sql.VarChar, currentStatus);
        conn.input('garbageEntity7', sql.Bit, garbageEntity);
        conn.input('endTimeVideoIndexer7', sql.DateTime, endTimeVideoIndexer);
        conn.input('videoDurationInSeconds7', sql.Float, videoDurationInSeconds);
        conn.input('detectedMajorSentimentVideo7', sql.NVarChar, detectedMajorSentimentVideo);
        conn.input('detectedMajorSentimentVideoScore7', sql.Float, detectedMajorSentimentVideoScore*100);
        conn.input('detectedMajorEmotionVideo7', sql.NVarChar, detectedMajorEmotionVideo);
        conn.input('detectedMajorEmotionVideoScore7', sql.Float, detectedMajorEmotionVideoScore*100);
        conn.input('transcriptText7', sql.NVarChar, JSON.stringify(transcriptText));
        conn.input('keywords7', sql.NVarChar, JSON.stringify(keywords));
        conn.input('labels7', sql.NVarChar, JSON.stringify(labels));
        conn.input('scenes7', sql.NVarChar, JSON.stringify(scenes));
        conn.input('brands7', sql.NVarChar, JSON.stringify(brands));
        conn.input('topics7', sql.NVarChar, JSON.stringify(topics));
        conn.input('videoStatistics7', sql.NVarChar, JSON.stringify(videoStatistics));
        conn.input('audioEffects7', sql.NVarChar, JSON.stringify(audioEffects));
        conn.input('sourceLanguage7', sql.VarChar, sourceLanguage);
        conn.input('language7', sql.VarChar, language);
        conn.input('detectedOrgId7', sql.NVarChar, detectedOrgId);
        conn.input('entityName7', sql.NVarChar, entityName)
        conn.input('conclusiveSentiment7', sql.VarChar, conclusiveSentiment)
        conn.input('conclusiveSentimentScore7', sql.Float, conclusiveSentimentScore?conclusiveSentimentScore*100:0.5*100)

        
        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        currentStatus=@currentStatus7,
        statusCompleted=@statusCompleted7,
        garbageEntity=@garbageEntity7,
        endTimeVideoIndexer=@endTimeVideoIndexer7,
        videoDurationInSeconds=@videoDurationInSeconds7,
        detectedMajorSentimentVideo=@detectedMajorSentimentVideo7,
        detectedMajorSentimentVideoScore=@detectedMajorSentimentVideoScore7,
        detectedMajorEmotionVideo=@detectedMajorEmotionVideo7,
        detectedMajorEmotionVideoScore=@detectedMajorEmotionVideoScore7,
        transcriptText=@transcriptText7,
        keywords=@keywords7,
        labels=@labels7,
        brands=@brands7,
        scenes=@scenes7,
        topics=@topics7,
        audioEffects=@audioEffects7,
        videoStatistics=@videoStatistics7,
        language=@language7,
        detectedOrgId=@detectedOrgId7,
        entityName=@entityName7,
        sourceLanguage=@sourceLanguage7,
        conclusiveSentiment=@conclusiveSentiment7,
        conclusiveSentimentScore=@conclusiveSentimentScore7
        WHERE 
        interactionId=@interactionId7`
    }

    if(currentStatus=="SUBMIT"){
        conn.input('interactionId8', sql.VarChar, interactionId);
        conn.input('customerProvidedSentiment8', sql.VarChar, customerProvidedSentiment)
        conn.input('nationality8', sql.VarChar, nationality);
        conn.input('gender8', sql.VarChar, gender);
        conn.input('age8', sql.Float, age);
        conn.input('detectedMajorSentimentFace8', sql.NVarChar, detectedMajorSentimentFace)
        conn.input('detectedMajorSentimentFaceScore8', sql.Float, detectedMajorSentimentFaceScore)
        
        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        customerProvidedSentiment=@customerProvidedSentiment8,
        age=@age8,
        gender=@gender8,
        nationality=@nationality8,
        detectedMajorSentimentFace=@detectedMajorSentimentFace8,
        detectedMajorSentimentFaceScore=@detectedMajorSentimentFaceScore8
        WHERE 
        interactionId=@interactionId8`
    }

    if(currentStatus=="PERSON"){
        conn.input('interactionId9', sql.VarChar, interactionId);
        conn.input('detectedPersonID9', sql.NVarChar, detectedPersonID);
        conn.input('isVIPPerson9', sql.Bit, isVIPPerson);
        conn.input('gender9', sql.VarChar, gender);
        conn.input('age9', sql.Float, age);
        conn.input('detectedMajorEmotionFace9', sql.NVarChar, detectedMajorEmotionFace)
        conn.input('detectedMajorEmotionFaceScore9', sql.Float, detectedMajorEmotionFaceScore)
        
        query=`UPDATE digital_ajman.dbo.interaction 
        SET 
        detectedPersonID=@detectedPersonID9,
        isVIPPerson=@isVIPPerson9,
        age=@age9,
        gender=@gender9,
        detectedMajorEmotionFace=@detectedMajorEmotionFace9,
        detectedMajorEmotionFaceScore=@detectedMajorEmotionFaceScore9
        WHERE 
        interactionId=@interactionId9`
    }




    console.log("query------------------------->",query)

    return [query,conn];
}


module.exports = {
  insertInteraction,
  updateInteraction,
}