'use strict';

module.exports = {
    Payment: require('./Payment'),
    CustomerAssociation: require('./CustomerAssociation')
};
