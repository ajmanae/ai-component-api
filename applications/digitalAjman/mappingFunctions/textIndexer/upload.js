'use strict'
const pg = require('../../../../core/api/connectors/postgress');
const rp = require('request-promise');

async function upload(payload, UUIDKey, route, callback, JWToken) {
   console.log("payload",payload);
   try {
    let key = '00ef3af5852c4e918ca53b09a376a5b4';
    let uri= 'https://avanzainnovation.cognitiveservices.azure.com/text/analytics/v3.1-preview.1/sentiment';

    //uplaoding text

    var textOptions = {
        method: 'POST',
        uri: uri,
        body: payload.textData,
        headers: {
           'Content-Type': 'application/json',
           'Ocp-Apim-Subscription-Key': key,
        },
        json:true
    };

    let textResponse = await rp(textOptions);

    console.log("text Response------------>",textResponse);

    let detailResponse ={
        tranxdata:JSON.stringify(textResponse),
        majorSentiment:textResponse.documents[0].sentiment,
        indexId:`${new Date().getTime()}`,
      };

      let query = `INSERT INTO smiles.public.textuploads (indexid , tranxdata, majorsentiment) 
        VALUES ('${detailResponse.indexId}', '${detailResponse.tranxdata}', '${detailResponse.majorSentiment}')`
      console.log("query", query);
      pg.connection().then((conn) => {
         return Promise.all([
            conn.query(query)
         ]).then((data) => {
            console.log(data)
            callback({
               ...detailResponse,
               success:true
            });
         });
      });
   } catch (err) {
      return callback({...err,success:false});
   }
}
exports.upload = upload;