'use strict'
const sqlserver = require('../../../../core/api/connectors/mssql')
const { json } = require('sequelize')
const moment = require('moment')
const _ = require('lodash')

exports.getInteractionList = async function (
  payload,
  UUIDKey,
  route,
  callback,
  JWToken
) {
  // console.log('payload', payload)

  console.log('payload getInteractionList-------->>', payload)
  let searchCriteria = payload.searchCriteria
  let page = payload.page
  try {
    let query
    query = `SELECT i2.*,p.thumbnailPath, p.name
    FROM interaction i2 
    LEFT JOIN person p ON i2.detectedPersonID = p.personId`
    let where_clause = ''
    where_clause = `where 1=1 and currentStatus NOT LIKE 'TEMP'`

    if (Object.entries(searchCriteria).length > 0) {
      for (let i = 0; i < Object.keys(searchCriteria).length; i++) {
        if(Object.keys(searchCriteria)[i] === 'detectedMajorEmotionVideo'){
          where_clause += ` and lower(type) NOT LIKE lower('T')`
        }
        // if(Object.keys(searchCriteria)[i] === 'interactionId'){
        //   where_clause += ` and lower(i2.interactionId) LIKE ${searchCriteria[Object.keys(searchCriteria)[i]]}`
        // }
        where_clause += ` and LOWER(${(Object.keys(searchCriteria)[i])}) LIKE lower('%${
          searchCriteria[Object.keys(searchCriteria)[i]]
        }%') `
      }
    }

    console.log("where ================== " + where_clause);

    let query1 = 'SELECT count(*) as count FROM digital_ajman.dbo.interaction i2'
        query1 = query1 + ' ' + where_clause
    
        console.log("query1 ================== " + query1);
    let conn = await sqlserver.connection()
    let data1 = await conn.query(query1)
    console.log("Data 1 =============== ", data1);
    let count = _.get(data1, 'recordset[0].count', 0);
    let currentPageNo = payload.page.currentPageNo
    let pageSize = payload.page.pageSize
    if(count != 0 && !(count > (pageSize*(currentPageNo-1)) )){ 
      currentPageNo = _.ceil(count/10);
    }
    let offset = pageSize * (currentPageNo - 1)
    let pagination = ` ORDER By submissionTime DESC OFFSET ${offset} ROWS FETCH NEXT ${pageSize} ROWS ONLY`
    query = query + ' ' + where_clause + ' ' + pagination


    console.log('Connected to DB successfully !')
    let data = await conn.query(query)

    
    // console.log('count query ==========>>>>>>>>>> ', query1)

    let interactionList = _.get(data, 'recordsets[0]', [])
     console.log('final query ===========', query)

    
    // console.log("interaction List Data ------------=-=-=-=-=-",JSON.stringify(data1))
    // console.log("interactionList[0].totalRecord= ===== ", interactionList[0].totalRecord)
    console.log("data1 =============== ", data1)
    const finalResponse = {
      messageStatus: 'OK',
      errorDescription: 'Processed OK!',
      errorCode: 200,
      timestamp: moment().format('DD/MM/YYY hh:mm:ss.SSS'),
      interactionList: {
        data: interactionList,
        page: {
          pageSize: payload.page.pageSize,
          currentPageNo: currentPageNo,
          totalRecords: _.get(data1, 'recordset[0].count', 0)
        }
      }
    }
    // console.log("final response ==== ",finalResponse );
    return callback(finalResponse)
  } catch (err) {
    return callback(err)
  }
}

exports.getInteractionById = function (
  payload,
  UUIDKey,
  route,
  callback,
  JWToken
) {
  // console.log('payload', payload)

  console.log('payload getInteractionById-------->>', payload)

  try {
    let query = `SELECT i2.*,p.thumbnailPath, p.name
    FROM interaction i2 
    LEFT JOIN person p ON i2.detectedPersonID = p.personId where i2.interactionId = '${payload.interactionId}'`
    console.log('getInteractionById query ===========', query)

    sqlserver.connection().then(async conn => {
      console.log('Connected to DB successfully !')
      return Promise.all([conn.query(query)]).then(data => {
        const finalResponse = {
          messageStatus: 'OK',
          errorDescription: 'Processed OK!',
          errorCode: 200,
          timestamp: moment().format('DD/MM/YYY hh:mm:ss.SSS'),
          interactionRecord: {
            data: data[0].recordset[0]
          }
        }
        // console.log("final response ==== ",finalResponse );
        return callback(finalResponse)
      })
    })
  } catch (err) {
    return callback(err)
  }
}

exports.getPersonImagePath = function (
  payload,
  UUIDKey,
  route,
  callback,
  JWToken
) {
  // console.log('payload', payload)

  console.log('payload getPersonImagePath-------->>', payload)

  try {
    let query = `SELECT thumbnailPath FROM digital_ajman.dbo.person where interactionId = '${payload.interactionId}'`
    console.log('getPersonImagePath query ===========', query)

    sqlserver.connection().then(async conn => {
      console.log('Connected to DB successfully !')
      return Promise.all([conn.query(query)]).then(data => {
        const finalResponse = {
          messageStatus: 'OK',
          errorDescription: 'Processed OK!',
          errorCode: 200,
          timestamp: moment().format('DD/MM/YYY hh:mm:ss.SSS'),
          personDetails: {
            data: data[0].recordset[0]
          }
        }
        // console.log("final response ==== ",finalResponse );
        return callback(finalResponse)
      })
    })
  } catch (err) {
    return callback(err)
  }
}

exports.updateInteractionTileData = function (
  payload,
  UUIDKey,
  route,
  callback,
  JWToken
) {
  // console.log('payload', payload)
  console.log('payload getPersonImagePath-------->>', payload)
  let updateBody = payload.updatedData
  try {
    let set = `set ${Object.keys(updateBody)[0]} = '${
      updateBody[Object.keys(updateBody)[0]]
    }'`
    for (let i = 1; i < Object.keys(updateBody).length; i++) {
      // if(Object.keys(updateBody)[i] == "age"){
      //     set += `, '${Object.keys(updateBody)[i]}' = ${updateBody[Object.keys(updateBody)[i]]}`;
      // }
      set += `, ${Object.keys(updateBody)[i]} = '${
        updateBody[Object.keys(updateBody)[i]]
      }'`
    }
    let query = `update digital_ajman.dbo.interaction ${set} where interactionId = '${payload.interactionId}'`
    console.log('updateInteractionTileData query ===========', query)

    sqlserver.connection().then(async conn => {
      console.log('Connected to DB successfully !')
      return Promise.all([conn.query(query)]).then(data => {
        // console.log("data=== = = = = = = ", data);
        const finalResponse = {
          messageStatus: 'OK',
          errorDescription: 'Processed OK!',
          errorCode: 200,
          timestamp: moment().format('DD/MM/YYY hh:mm:ss.SSS'),
          personDetails: {
            data: data
          }
        }
        // console.log("final response ==== ",finalResponse );
        return callback(finalResponse)
      })
    })
  } catch (err) {
    return callback({
      messageStatus: 'ERROR',
      errorDescription: 'Unable to Process'
    })
  }
}


