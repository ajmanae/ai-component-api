'use strict'
const config = require('../../../../config/index')
const path = require('path')
const fs = require('fs')
const sha512 = require('../../../../lib/hash/sha512')
const { create, findDocument } = require('../../../../lib/services/documents')
const ServerFS = require('../../../../core/mappingFunctions/fileUploadEx/server-fs')

module.exports = async function upload (payload, UUIDKey, JWToken) {
  let userID, type, source, fileReference
  source = 'API'
  type = 'FILE'
  //   if (JWToken.userID) {
  //     userID = JWToken.userID;
  //   }

  const fileName = payload.files.file.name
  const fileExtension = fileName.split('.').pop()

  console.log(fileName, 'File Name----------------------->')
  console.log(fileExtension, 'File Extension-------------------->')

  let fsObject = new ServerFS()

  try {
    const fileHash = sha512(payload.files.file.data)
    let filePath = null
    let resp = {}
    // if (!payload.files.file['mv']) {
      const basePath = String(config.get('basePath'))
      const completeBasePath = path.normalize(path.join(basePath))
      const completePath = path.normalize(
        path.join(completeBasePath, fileHash + '.' + fileExtension)
      )
      try {
        console.log("Writing File-------------->")
        await fs.writeFileSync(completePath, payload.files.file.data) // need to be in an async function
      } catch (fileError) {
        console.log(
          'Error in while writing the file------------------>',
          fileError
        )
      }
      filePath = completePath
    // } else {
    //   filePath = await fsObject.upload(payload.files.file, fileHash)
    // }

    let fileCreated = await create({
      path: filePath,
      ext: fileExtension.toUpperCase(),
      name: fileName,
      type: type,
      userId: null,
      source: source,
      UUID: UUIDKey,
      hash: fileHash,
      context: '',
      contentType: payload.files.file.mimetype,
      fileReference: fileReference,
      policies: ''
    });

    console.log("File created Response------->",fileCreated)

    let downloadPath = `/API/core/downloadEx?type=${type}&path=${fileHash}`
    console.log(downloadPath, 'Download Path----------------------------->')
    return { downloadPath, filePath }
  } catch (err) {
    console.log(err, 'Error saving file locally----------------->')
  }
}
