'use strict'

const { config } = require('process')

const threadStart = async config => {
  const sqlserver = require('../../../../core/api/connectors/mssql')
  const mongoDB = require('../../../../core/api/connectors/mongoDB')

  const rp = require('request-promise')
  const moment = require('moment')
  const stringSimilarity = require('string-similarity');
  const languageIndexer = require('./languageIndexer')
  const keyphraseIndexer = require('./keyphraseIndexer')
  const NERIndexer = require('./nerIndexer')
  const textIndexer = require('./textIndexer')
  const languageConverter = require('./languageConvertor')
  const personIndexing = require('./personIndexing')
  const db = require('../../../../core/database/db')()
  const _ = require('lodash')
  console.log('PROCESS MONGO>', _.get(config, 'mongodb.url', undefined))
  mongoDB.connection(_.get(config, 'mongodb.url', undefined))

  const fs = require('fs')
  const {
    insertInteraction,
    updateInteraction
  } = require('../../lib/queries.js')

  let spinner = async function () {
    console.log('quering------------------------------------->')
    try {
      let connection = await sqlserver.connection();
      // let time = moment().subtract(15,'minutes').format('YYYY-HH-MM HH:mm:ss')
      let responseData = await connection.query(
        `select TOP 15 JSON_VALUE(videoindexerUploadResponse, '$.id') as videoid,JSON_VALUE(videoindexerUploadResponse, '$.sourceLanguage') as videoLanguage, * from digital_ajman.dbo.interaction where currentStatus not in ('COMPLETED', 'TEMP') ORDER BY internalid DESC`
      )
      responseData.recordset.forEach(async elem => {
        if(!elem.jobRunning){
          console.log("job started--------->>>>>>>>>>>>>>");
          if (elem.type == 'T') {
            await textService(
              elem.interactionId,
              {
                documents: [
                  {
                    id: 1,
                    text: elem.providedText
                  }
                ]
              },
              elem.type,
              elem.orgId,
              elem.submissionTime,
              elem.convertedText?JSON.parse(elem.convertedText):null,
              elem.isProcessed,
            )
          } else {
            await indexService(
              elem.interactionId,
              elem.videoid,
              elem.type,
              elem.detectedPersonID,
              elem.orgId,
              elem.detectedMajorSentimentFace,
              elem.detectedMajorSentimentFaceScore?elem.detectedMajorSentimentFaceScore/100:0,
              elem.convertedText?JSON.parse(elem.convertedText):null,
              elem.videoLanguage,
              elem.transcriptText?JSON.parse(elem.transcriptText):null,
              elem.submissionTime,
              elem.isProcessed
            )
          }
        }
      })
      connection.thisConnPool.close()
      setTimeout(spinner, 80000)
    } catch (ex) {
      console.log(ex,"Threadd catchhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh")
      setTimeout(spinner, 80000)
    }
  }

  try {
    setTimeout(spinner, 80000)
  }catch(sperr){
    console.log("spinner error---------------------->",sperr);
    setTimeout(spinner, 80000)
  }

  //hard config
  let accountId = '18c0e61d-d611-4526-86f7-ffe0f75fe9d5'
  let location = 'eastus'
  let key = '72c126cececc496a909566afb5f36441'
  let uri = 'https://api.videoindexer.ai'
  let getTokenUri = `${uri}/Auth/${location}/Accounts?generateAccessTokens=true&allowEdit=true`

  function getOrgsData () {
    return new Promise(resolve => {
      global.db.select('Entity', {}, '', function (err, orgsData) {
        if (err) {
          resolve(undefined)
        } else {
          resolve(orgsData)
        }
      })
    })
  }

  const languageMapping = {
    Arabic: 'ar-EG',
    English: 'en-US',
    Hindi:'hi-IN',
    Urdu:'ur-PK'
  }

  
const ALTkeywords={
  "Japan":"Ajman", 
  "Dutchmen":"Ajman",
  "Edge Man":"Ajman",
  "Magic Man":"Ajman",
  "German":"Ajman",
  "Judgement":"Ajman",
  "Man Free Zone":"Ajman Free Zone",
  "Ajman Ember":"Ajman Chamber",
  "advent chamber":"Ajman Chamber",
  "General Police":"Ajman Police",
  "When Departmeny":"Ajman Department",
  "Village Man":"Digital Ajman",
  "Josh Goardan Holiday":"Transport Authority",
  "Batman Protest":"Ajman Police",
  "customer services":"Custome Services",
  "Number":"Ajman Chamber",
  "Board and customs":"Port and Customs",
  "Department of lying":"Department of Land",
  "Peace be upon you":"",
  "Peace be upon you and god's mercy":"",
  "Peace be upon you and god mercy":"",
  "Peace be upon you god's mercy":"",
  "Peace be upon you god mercy":"",
  "Peace be upon you and god's mercy and blessing":"",
  "Peace be upon you and god mercy and blessing":"",
  "Peace be upon you and god's mercy and blessings":"",
  "Peace be upon you and god mercy and blessings":"",
  "Peace be upon you and god mercy blessings":"",
  "Peace be upon you god mercy blessings":"",
  "Peace be upon you god mercy blessing":"",
  "Peace and mercy of God be upon you":"",
  "Peace, mercy and blessings of God":"",
  "Peace and God's mercy and blessings":""
  

}

  // end config

  //text
  async function textService (
    interactionId,
    textData,
    type,
    orgId,
    submissionTime,
    convertedTextA,
    isProcessed
  ) {
    console.log(
      'args000----------------------->',
      interactionId,
      textData,
      type,
      orgId,
      submissionTime,
      convertedTextA
    )
    let connection = await sqlserver.connection()
    let responseData;
   
    if(isProcessed){
      let historySP = `EXEC interactionHistoryInsertion '${interactionId}'`;
      try{
        let historySPResponse = await connection.query(historySP);
        console.log(`historySP Response------------------------------>`,historySPResponse);
      }
      catch(historySPError){
        console.log(` Eror-------------------->`,historySPError);
        connection.thisConnPool.close()
      }
    }

    try {
      responseData =  await connection.query(
        `UPDATE digital_ajman.dbo.interaction SET jobRunning='${1}' WHERE interactionId='${interactionId}'`
      );
    } catch (err) {
      console.log('Job error>>>>>>>>>>>>', err)
      connection.thisConnPool.close()
    }

    let garbageEntity = 0
    let detectedOrgId = null
    let entityName = null
    let startTimeTextAnalytics = moment().format('YYYY-MM-DD HH:mm:ss')
    let providedText = textData.documents[0].text
    let detectedLanguage = await languageIndexer(textData)
    let startTimeLanguageConversion = moment().format('YYYY-MM-DD HH:mm:ss')
    let convertedText = convertedTextA?convertedTextA : await languageConverter(
      textData.documents,
      languageMapping[detectedLanguage ? detectedLanguage : 'Arabic']
        ? languageMapping[detectedLanguage ? detectedLanguage : 'Arabic']
        : 'ar-EG'
    )
    let endTimeLanguageConversion = moment().format('YYYY-MM-DD HH:mm:ss')
    console.log('converted Text----------------------------->', convertedText)
    let currentStatus = 'ANALYZING KEYPHRASES'
    let statusCompleted = ['RECEIVED', 'DETECTING LANGUAGE']
    let [keypharseQuery, keypharseConn] = insertInteraction(
      {
        interactionId,
        type,
        garbageEntity,
        orgId,
        detectedLanguage,
        currentStatus,
        submissionTime,
        providedText,
        startTimeTextAnalytics,
        statusCompleted,
        startTimeLanguageConversion,
        endTimeLanguageConversion,
        convertedText:convertedText?convertedText:[]
      },
      connection
    )

    try {
      responseData = await keypharseConn.query(keypharseQuery)
    } catch (err) {
      console.log(err, 'DETECTING LANGUAGE error>>>>>>>>>>>>', err)
      connection.thisConnPool.close()
    }

    currentStatus = 'RECOGNIZING ENTITIES'
    statusCompleted.push('ANALYZING KEYPHRASES')
    let keyphrases = await keyphraseIndexer({
      documents: convertedText
    })
    let [REQuery, REConn] = updateInteraction(
      {
        interactionId,
        currentStatus,
        keyphrases,
        statusCompleted
      },
      connection
    )

    try {
      responseData = await REConn.query(REQuery)
    } catch (err) {
      console.log(err, 'ANALYZING KEYPHRASES error>>>>>>>>>>>>', err)
      connection.thisConnPool.close()
    }

    let orgsList = await getOrgsData()
    currentStatus = 'ANALYZING TEXT'
    statusCompleted.push('RECOGNIZING ENTITIES')
    let NER = await NERIndexer({
      documents: convertedText
    })
    let ndetectedOrgId = null
    let ndetectedOrgIdConfidence = 0
    if (NER.length) {
      NER.forEach(entity => {
        if(entity.category=="Organization" || entity.category=="PersonType" ){
        if (entity.confidenceScore > ndetectedOrgIdConfidence) {
          ndetectedOrgIdConfidence = entity.confidenceScore
          ndetectedOrgId = entity.text
        }
        }
      })
      if(ndetectedOrgId){
        orgsList.forEach(org => {
          console.log(org.arabicName.toLowerCase(),ndetectedOrgId.toLowerCase(),stringSimilarity.compareTwoStrings(org.arabicName.toLowerCase(),ndetectedOrgId.toLowerCase()) ,"NERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
          if (
            // _.includes(org.arabicName.toLowerCase(), ndetectedOrgId.toLowerCase())
            stringSimilarity.compareTwoStrings(org.arabicName.toLowerCase(),ndetectedOrgId.toLowerCase())>=0.5
          ) {
            detectedOrgId = org.spCode
            entityName = org.arabicName
          }
        })
      }
    }
    let [textQuery, textConn] = updateInteraction(
      {
        interactionId,
        currentStatus,
        NER,
        statusCompleted
      },
      connection
    )

    try {
      responseData = await textConn.query(textQuery)
    } catch (err) {
      console.log(err, 'RECOGNIZING ENTITIES error>>>>>>>>>>>>', err)
      connection.thisConnPool.close()
    }

    currentStatus = 'COMPLETED'
    statusCompleted.push('ANALYZING TEXT')
    let textResponse = await textIndexer({
      documents: convertedText
    })
    let endTimeTextAnalytics = moment().format('YYYY-MM-DD HH:mm:ss')
    let detectedMajorSentimentText =
      textResponse.documents[0].sentiment == 'mixed'
        ? 'neutral'
        : textResponse.documents[0].sentiment
    let detectedMajorSentimentTextScore =
      textResponse.documents[0].confidenceScores[detectedMajorSentimentText]
    let textAnalyticsResponse = textResponse
    let opinionsAndAspects = []
    textResponse.documents[0].sentences.forEach(sentence => {
      if (sentence.aspects.length) {
        opinionsAndAspects = sentence.aspects.map(aspect => {
          return {
            ...aspect,
            opinions: aspect.relations.map(relation => {
              let index = relation.ref.split('/')
              return sentence.opinions[Number(index[index.length - 1])]
            })
          }
        })
      }
    })
    if(!detectedOrgId){
      detectedOrgId=orgId;
      entityName= orgsList.filter(org=>org.spCode==orgId).length?orgsList.filter(org=>org.spCode==orgId)[0].arabicName:null
    }
    console.log("entity Name------------------------------>",entityName);
    let conclusiveSentiment=detectedMajorSentimentText?detectedMajorSentimentText:"neutral";
    let conclusiveSentimentScore = detectedMajorSentimentTextScore;
    let [completeQuery, completeConn] = updateInteraction(
      {
        interactionId,
        type,
        currentStatus,
        garbageEntity,
        statusCompleted,
        endTimeTextAnalytics,
        detectedMajorSentimentText,
        detectedMajorSentimentTextScore:detectedMajorSentimentTextScore?detectedMajorSentimentTextScore:0.5,
        textAnalyticsResponse,
        detectedOrgId,
        opinionsAndAspects,
        conclusiveSentiment,
        conclusiveSentimentScore,
        entityName
      },
      connection
    )

    try {
      responseData = await completeConn.query(completeQuery)
      // connection.thisConnPool.close()
      // return { id: interactionId, success: true }
    } catch (err) {
      console.log(err, 'ANALYZING TEXT error>>>>>>>>>>>>', err)
      connection.thisConnPool.close()
    }

    // let storedProcedure = ["happinessSummaryInsertion","vipInsertion","insertAspectsAndOpinions","emotionInsertion","languageWiseSentimentInsertion"];
    let storedProcedure = _.get(config, 'sp_digitalAjman', []);
    storedProcedure.forEach(async sp=>{
      let spQuery = `EXEC ${sp} '${interactionId}','API'`;
            console.log("SP Query----------------------->",spQuery);
            if(sp=="vipInsertion" && (moment().format("YYYY-MM-DD")==moment(submissionTime).format("YYYY-MM-DD")) ){
              console.log("calling vipInsertion");
              try{
                let spResponse = await connection.query(spQuery);
                console.log(`${sp} Response------------------------------>`,spResponse);
              }
              catch(spError){
                console.log(`${sp} Eror-------------------->`,spError);
                connection.thisConnPool.close()
              }
            }
            if(sp!="vipInsertion"){
              try{
                let spResponse = await connection.query(spQuery);
                console.log(`${sp} Response------------------------------>`,spResponse);
              }
              catch(spError){
                console.log(`${sp} Eror-------------------->`,spError);
                connection.thisConnPool.close()
              }
            }
      
    })

    let procedureQuery = `UPDATE digital_ajman.dbo.interaction SET isProcessed='${1}',jobRunning='${0}' WHERE interactionId='${interactionId}'`;
    try{
      let procedureResponse = await connection.query(procedureQuery);
      console.log(`Procedure Update Response------------------------------>`,procedureResponse);
      connection.thisConnPool.close()
      return { id: interactionId, success: true }
    }
    catch(pError){
      console.log(`Procedure update Eror-------------------->`,pError);
      connection.thisConnPool.close()
    }
  }

  //video/audio
  async function indexService (
    interactionId,
    videoId,
    type,
    detectedPersonID,
    orgId,
    detectedMajorSentimentFace,
    detectedMajorSentimentFaceScore,
    convertedTextA,
    sourceLanguageA,
    transcriptTextA,
    submissionTime,
    isProcessed
  ) {
    console.log(
      'args000----------------------->',
      interactionId,
      videoId,
      type,
      detectedPersonID,
      orgId,
      detectedMajorSentimentFace,
      detectedMajorSentimentFaceScore,
      convertedTextA,
      sourceLanguageA,
      transcriptTextA,
      submissionTime
    )
    //   setTimeoƒut(async () => {
    try {
      var getTokenOptions = {
        method: 'GET',
        uri: getTokenUri,
        headers: {
          'Ocp-Apim-Subscription-Key': key
        },
        json: true
      }

      //get access token
      let tokenResponse = await rp(getTokenOptions)
      console.log(tokenResponse, 'Token Response')
      let accessToken = tokenResponse[tokenResponse.length - 1].accessToken

      //indexing video
      let IndexUri = `https://api.videoindexer.ai/${location}/Accounts/${accountId}/Videos/${videoId}/Index?language=${sourceLanguageA=="hi-IN"?"ur-PK":sourceLanguageA}&reTranslate=true&accessToken=${accessToken}`
      console.log(IndexUri, 'Index URI')

      var IndexOptions = {
        method: 'GET',
        uri: IndexUri,
        headers: {
          'Ocp-Apim-Subscription-Key': key
        },
        json: true
      }

      let IndexResponse = await rp(IndexOptions)

      if (IndexResponse.state == 'Processed') {
        console.log('Index Response------------>', IndexResponse);

        let connection = await sqlserver.connection();
        let responseData
       

        if(isProcessed){
          let historySP = `EXEC interactionHistoryInsertion '${interactionId}'`;
          try{
            let historySPResponse = await connection.query(historySP);
            console.log(`historySP Response------------------------------>`,historySPResponse);
          }
          catch(historySPError){
            console.log(` Eror-------------------->`,historySPError);
            connection.thisConnPool.close()
          }
        }

        try {
          responseData =  await connection.query(
            `UPDATE digital_ajman.dbo.interaction 
            SET 
            jobRunning='1' WHERE 
            interactionId='${interactionId}'`
          );
        } catch (err) {
          console.log(err, 'Job error>>>>>>>>>>>>', err)
          connection.thisConnPool.close()
        }
        
        if (
          IndexResponse.summarizedInsights.faces.length &&
          !detectedPersonID
        ) {
          await personIndexing(IndexResponse.summarizedInsights.faces,interactionId);
            // UUIDKey,
            // JWToken
        }
        let garbageEntity = 0
        let endTimeVideoIndexer = moment().format('YYYY-MM-DD HH:mm:ss')
        let videoDurationInSeconds =
          IndexResponse.summarizedInsights.duration.seconds
        let detectedMajorSentimentVideo = null
        let detectedMajorSentimentVideoScore = 0
        let detectedMajorEmotionVideo = null
        let detectedMajorEmotionVideoScore = 0
        let transcriptText = transcriptTextA?transcriptTextA:IndexResponse.videos[0].insights['transcript']
          ? IndexResponse.videos[0].insights.transcript
          : []
        let keywords = IndexResponse.summarizedInsights.keywords
        let labels = IndexResponse.summarizedInsights.labels.map(
          label => label.name
        )
        let brands = IndexResponse.videos[0].insights['brands']
          ? IndexResponse.videos[0].insights.brands
          : []
        let topics = IndexResponse.summarizedInsights.topics
        let audioEffects = IndexResponse.summarizedInsights.audioEffects
        let videoStatistics = IndexResponse.summarizedInsights.statistics
        let scenes = IndexResponse.videos[0].insights.scenes
        let language = IndexResponse.videos[0].insights.language
        let sourceLanguage = IndexResponse.videos[0].insights.sourceLanguage
        let detectedOrgId = null
        let entityName = null
        let orgsList = await getOrgsData()
        console.log('brands--------------------->', JSON.stringify(brands))
        let bdetectedOrgId = null
        let bdetectedOrgIdConfidence = 0
        if (brands.length) {
          brands.forEach(brand => {
            if (brand.confidence > bdetectedOrgIdConfidence) {
              bdetectedOrgIdConfidence = brand.confidence
              bdetectedOrgId = brand.tags[0]
            }
          })

          orgsList.forEach(org => {
            console.log(
              org.spCode,
              bdetectedOrgId,
              'ORRRRGGGGGGGGGGGGGGGGGGGGGGGGGGGG'
            )
            if (bdetectedOrgId == org.spCode) {
              detectedOrgId = org.spCode
              entityName = org.arabicName
            }
          })
        }

        let AllSentiments = {
          Neutral: 0,
          Positive: 0,
          Negative: 0
        }

        let AllSentiments1 = {
          neutral: 0,
          positive: 0,
          negative: 0
        }

        let AllEmotions = {
          Anger: 0,
          Joy: 0,
          Fear: 0,
          Sad: 0
        }

        IndexResponse.summarizedInsights.sentiments.forEach(sentiment => {
          AllSentiments[sentiment.sentimentKey] = sentiment.seenDurationRatio
          //   sentiment.appearances.forEach(appearance => {
          //  AllSentiments[sentiment.sentimentKey] += appearance.endSeconds - appearance.startSeconds
          //   });
        })

        console.log(AllSentiments, 'ALL Senitments-------------->')

        Object.keys(AllSentiments).map(o => {
          if (AllSentiments[o] > detectedMajorSentimentVideoScore) {
            detectedMajorSentimentVideoScore = AllSentiments[o]
            detectedMajorSentimentVideo = o.toLowerCase();
          }
        })

        console.log(
          detectedMajorSentimentVideo,
          'Major Sentiment--------------------------->'
        )

        IndexResponse.summarizedInsights.emotions.forEach(emotion => {
          AllEmotions[emotion.type] = emotion.seenDurationRatio
          //  emotion.appearances.forEach(appearance => {
          //     AllEmotions[emotion.type] += appearance.endSeconds - appearance.startSeconds
          //  });
        })

        console.log(AllEmotions, 'ALL Emotions-------------->')

        Object.keys(AllEmotions).map(o => {
          if (AllEmotions[o] > detectedMajorEmotionVideoScore) {
            detectedMajorEmotionVideoScore = AllEmotions[o]
            detectedMajorEmotionVideo = o.toLowerCase();
          }
        })

        console.log(detectedMajorEmotionVideo, 'Major Emotion>')

       

      if (transcriptText.length) {
          let currentStatus = 'DETECTING LANGUAGE'
          let statusCompleted = [
            'RECEIVED',
            type == 'V' ? 'ANALYZING VIDEO' : 'ANALYZING AUDIO'
          ]
          let startTimeTextAnalytics = moment().format('YYYY-MM-DD HH:mm:ss')
          let [languageQuery, languageConn] = updateInteraction(
            {
              interactionId,
              currentStatus,
              statusCompleted,
              garbageEntity,
              startTimeTextAnalytics,
              endTimeVideoIndexer,
              videoDurationInSeconds,
              detectedMajorSentimentVideo,
              detectedMajorSentimentVideoScore:detectedMajorSentimentVideoScore?detectedMajorSentimentVideoScore:0.5,
              detectedMajorEmotionVideo,
              detectedMajorEmotionVideoScore:detectedMajorEmotionVideoScore?detectedMajorEmotionVideoScore:0.5,
              detectedOrgId,
              transcriptText,
              keywords,
              labels,
              brands,
              scenes,
              topics,
              audioEffects,
              videoStatistics,
              language,
              sourceLanguage
            },
            connection
          )

          try {
            responseData = await languageConn.query(languageQuery)
          } catch (err) {
            console.log(err, 'ANALYZING VIDEO/AUDIO error>>>>>>>>>>>>', err)
            connection.thisConnPool.close()
            // indexService(interactionId, videoId)
          }

          currentStatus = 'ANALYZING KEYPHRASES'
          statusCompleted.push('DETECTING LANGUAGE')

          let detectedLanguage = IndexResponse.videos[0].insights['transcript']
            ? await languageIndexer({
                documents: IndexResponse.videos[0].insights.transcript.map(
                  transcript => {
                    return {
                      id: transcript.id,
                      text: transcript.text
                    }
                  }
                )
              })
            : null;

            let startTimeLanguageConversion = moment().format('YYYY-MM-DD HH:mm:ss')
            let convertedText = convertedTextA? convertedTextA : await languageConverter(
              transcriptText,
              languageMapping[detectedLanguage ? detectedLanguage : 'Arabic']
              ? languageMapping[detectedLanguage ? detectedLanguage : 'Arabic']
              : 'ar-EG'
            );
            console.log("converted Text for analysis>>>>>>>>>>>>>>>>>>>>>>>>>>>",convertedText);
            let endTimeLanguageConversion = moment().format('YYYY-MM-DD HH:mm:ss')
            let text = "";
            if(convertedText){
              convertedText.forEach(t=>{
                text=`${text} ${t.text}`
              });
              Object.keys(ALTkeywords).forEach(keyword=>{
                let re = new RegExp(keyword,"gi");
                // convertedText[0].text = convertedText[0].text.replace(re, ALTkeywords[keyword]) 
                text = text.replace(re, ALTkeywords[keyword]) 
              })
            }
      
            // console.log('Altered converted Text-------------->', convertedText);


           

            console.log("text------------------>",text);
            let newDoc = {
              "id":1,
              "text":text
            }

           
          let [keypharseQuery, keypharseConn] = updateInteraction(
            {
              interactionId,
              detectedLanguage,
              currentStatus,
              statusCompleted,
              convertedText,
              startTimeLanguageConversion,
              endTimeLanguageConversion,
              convertedText: convertedText ? convertedText : [],
            },
            connection
          )

          try {
            responseData = await keypharseConn.query(keypharseQuery)
          } catch (err) {
            console.log(err, 'DETECTING LANGUAGE error>>>>>>>>>>>>', err)
            connection.thisConnPool.close()
            // indexService(interactionId, videoId)
          }

          currentStatus = 'RECOGNIZING ENTITIES'
          statusCompleted.push('ANALYZING KEYPHRASES')
          let keyphrases = await keyphraseIndexer({
            documents: [newDoc]
          })
          let [REQuery, REConn] = updateInteraction(
            {
              interactionId,
              currentStatus,
              keyphrases,
              statusCompleted
            },
            connection
          )

          try {
            responseData = await REConn.query(REQuery)
          } catch (err) {
            console.log(err, 'ANALYZING KEYPHRASES error>>>>>>>>>>>>', err)
            connection.thisConnPool.close()
            //   indexService(interactionId, videoId)
          }

          currentStatus = 'ANALYZING TEXT'
          statusCompleted.push('RECOGNIZING ENTITIES')
          let NER = await NERIndexer({
            documents:  [newDoc]
          })
          let ndetectedOrgId = null
          let ndetectedOrgIdConfidence = 0
          if (NER.length && !detectedOrgId) {
            NER.forEach(entity => {
              if(entity.category=="Organization" || entity.category=="PersonType" ){
              if (entity.confidenceScore > ndetectedOrgIdConfidence) {
                ndetectedOrgIdConfidence = entity.confidenceScore
                ndetectedOrgId = entity.text
              }
              }
            })
            if(ndetectedOrgId){
              orgsList.forEach(org => {
                console.log(org.arabicName.toLowerCase(),ndetectedOrgId.toLowerCase(),  stringSimilarity.compareTwoStrings(org.arabicName.toLowerCase(),ndetectedOrgId.toLowerCase()),"NERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                if (
                  stringSimilarity.compareTwoStrings(org.arabicName.toLowerCase(),ndetectedOrgId.toLowerCase())>=0.5
                ) {
                  detectedOrgId = org.spCode
                  entityName = org.arabicName
                }
              })
            }
          }
          let [textQuery, textConn] = updateInteraction(
            {
              interactionId,
              currentStatus,
              NER,
              statusCompleted
            },
            connection
          )

          try {
            responseData = await textConn.query(textQuery)
          } catch (err) {
            console.log(err, 'RECOGNIZING ENTITIES error>>>>>>>>>>>>', err)
            connection.thisConnPool.close()
            // indexService(interactionId, videoId)
          }

          currentStatus = 'COMPLETED'
          statusCompleted.push('ANALYZING TEXT')
          let textResponse = await textIndexer({
            documents:  [newDoc]
          })
          let endTimeTextAnalytics = moment().format('YYYY-MM-DD HH:mm:ss')
          let detectedMajorSentimentText = null
          let detectedMajorSentimentTextScore = 0
          let textAnalyticsResponse = textResponse
          let opinionsAndAspects = []
          textResponse.documents.forEach(document => {
            AllSentiments1[
              document.sentiment == 'mixed' ? 'neutral' : document.sentiment
            ] +=
              document.confidenceScores[
                document.sentiment == 'mixed' ? 'neutral' : document.sentiment
              ]
            document.sentences.forEach(sentence => {
              if (sentence.aspects.length) {
                opinionsAndAspects = sentence.aspects.map(aspect => {
                  return {
                    ...aspect,
                    opinions: aspect.relations.map(relation => {
                      let index = relation.ref.split('/')
                      return sentence.opinions[Number(index[index.length - 1])]
                    })
                  }
                })
              }
            })
          })

          console.log(AllSentiments1, 'ALL Senitments1-------------->')

          console.log(opinionsAndAspects, 'opinionsAndAspects-------------->')

          Object.keys(AllSentiments1).map(o => {
            if (AllSentiments1[o] > detectedMajorSentimentTextScore) {
              detectedMajorSentimentTextScore = AllSentiments1[o]
              detectedMajorSentimentText = o
            }
          })

          if(!detectedOrgId){
            detectedOrgId=orgId;
            entityName= orgsList.filter(org=>org.spCode==orgId).length?orgsList.filter(org=>org.spCode==orgId)[0].arabicName:null
          }
          detectedMajorSentimentText = detectedMajorSentimentText?detectedMajorSentimentText:"neutral";
          let conclusiveSentiment=null;
          let conclusiveSentimentScore=0;
          let totalSentiments = 0;
          if(detectedMajorSentimentVideo){
            totalSentiments +=1;
            conclusiveSentimentScore += detectedMajorSentimentVideoScore;
          }
          if(detectedMajorSentimentFace){
            totalSentiments +=1;
            conclusiveSentimentScore += detectedMajorSentimentFaceScore;
          }
          if(detectedMajorSentimentText){
            totalSentiments +=1;
            conclusiveSentimentScore += detectedMajorSentimentTextScore;
          }
          console.log("total Sentiments--------------------->",totalSentiments)
          console.log("conclusiveSentiment Score--------------------->",conclusiveSentimentScore);
          console.log(detectedMajorSentimentVideo,detectedMajorSentimentText,detectedMajorSentimentFace,"------------------>")
          let conclusiveSentimentCount=0; 
          if(detectedMajorSentimentVideo=="positive"){
            conclusiveSentimentCount += 1;
          }
          if(detectedMajorSentimentText=="positive"){
            conclusiveSentimentCount += 1;
          }
          if(detectedMajorSentimentFace=="positive"){
            conclusiveSentimentCount += 1;
          }
          console.log("conclusiveSentiment Count--------------------->",conclusiveSentimentCount)
          if(conclusiveSentimentCount >= 2 && type=="V"){
            console.log("setting positive Video-------------->")
            conclusiveSentiment="positive";
          }
          else if(conclusiveSentimentCount >= 1 && type=="A"){
            console.log("setting positive Audio-------------->")
            conclusiveSentiment="positive";
          }
          else{
            console.log("setting neutral-------------->")
            conclusiveSentiment="neutral";
          }

         
          if((detectedMajorSentimentVideo=="negative" || detectedMajorSentimentFace=="negative") && detectedMajorSentimentText!="positive"){
            conclusiveSentiment="negative";
          }
          if(detectedMajorSentimentText=="positive" || detectedMajorSentimentText=="negative"){
            conclusiveSentiment=detectedMajorSentimentText;
          }
          console.log("entity Name------------------------------>",entityName);
          let [completeQuery, completeConn] = updateInteraction(
            {
              interactionId,
              garbageEntity,
              currentStatus,
              statusCompleted,
              endTimeTextAnalytics,
              detectedMajorSentimentText,
              detectedMajorSentimentTextScore:detectedMajorSentimentTextScore?detectedMajorSentimentTextScore:0.5,
              textAnalyticsResponse,
              opinionsAndAspects,
              detectedOrgId,
              entityName,
              conclusiveSentiment,
              conclusiveSentimentScore:totalSentiments?conclusiveSentimentScore/totalSentiments:conclusiveSentimentScore/1
            },
            connection
          )

          try {
            responseData = await completeConn.query(completeQuery)
          } catch (err) {
            console.log(err, 'ANALYZING TEXT error>>>>>>>>>>>>', err)
            connection.thisConnPool.close()
            // indexService(interactionId, videoId)
          }

          // let storedProcedure = ["happinessSummaryInsertion","vipInsertion","insertAspectsAndOpinions","emotionInsertion","languageWiseSentimentInsertion"];
          let storedProcedure = _.get(config, 'sp_digitalAjman', []);
          setTimeout(async ()=>{
          storedProcedure.forEach(async sp=>{
            let spQuery = `EXEC ${sp} '${interactionId}','API'`;
            console.log("SP Query----------------------->",spQuery);
            if(sp=="vipInsertion" && (moment().format("YYYY-MM-DD")==moment(submissionTime).format("YYYY-MM-DD")) ){
              console.log("calling vipInsertion");
              try{
                let spResponse = await connection.query(spQuery);
                console.log(`${sp} Response------------------------------>`,spResponse);
              }
              catch(spError){
                console.log(`${sp} Eror-------------------->`,spError);
                connection.thisConnPool.close()
              }
            }
            if(sp!="vipInsertion"){
              try{
                let spResponse = await connection.query(spQuery);
                console.log(`${sp} Response------------------------------>`,spResponse);
              }
              catch(spError){
                console.log(`${sp} Eror-------------------->`,spError);
                connection.thisConnPool.close()
              }
            }
            
          })
      
          let procedureQuery = `UPDATE digital_ajman.dbo.interaction SET isProcessed='${1}',jobRunning='${0}' WHERE interactionId='${interactionId}'`;
          try{
            let procedureResponse = await connection.query(procedureQuery);
            console.log(`Procedure Update Response------------------------------>`,procedureResponse);
            connection.thisConnPool.close()
            return { id: interactionId, success: true }
          }
          catch(pError){
            console.log(`Procedure update Eror-------------------->`,pError);
            connection.thisConnPool.close()
          }
        },5000);

          // connection.thisConnPool.close()
      } else {
        console.log("GRARBAGEEEEEEEEEEEEEEEEEE");
          let currentStatus = 'COMPLETED'
          let statusCompleted = [
            'RECEIVED',
            type == 'V' ? 'ANALYZING VIDEO' : 'ANALYZING AUDIO'
          ]
          garbageEntity = 1;
          if(!detectedOrgId){
            detectedOrgId=orgId;
          }
          let conclusiveSentiment=null;
          let conclusiveSentimentScore=0;
          let totalSentiments = 0;
          if(detectedMajorSentimentVideo){
            totalSentiments +=1;
            conclusiveSentimentScore += detectedMajorSentimentVideoScore;
          }
          if(detectedMajorSentimentFace){
            totalSentiments +=1;
            conclusiveSentimentScore += detectedMajorSentimentFaceScore;
          } 
          let conclusiveSentimentCount=0;
          if(detectedMajorSentimentVideo=="positive"){
            conclusiveSentimentCount += 1;
          }
          if(detectedMajorSentimentFace=="positive"){
            conclusiveSentimentCount += 1;
          }
          if(conclusiveSentimentCount >= 1){
            conclusiveSentiment="positive";
          }
          else{
            conclusiveSentiment="neutral";
          }

          if(detectedMajorSentimentVideo=="negative" || detectedMajorSentimentFace=="negative"){
            conclusiveSentiment="negative";
          }
          console.log("total Sentiments--------------------->",totalSentiments)
          console.log("conclusiveSentiment Score--------------------->",conclusiveSentimentScore)
          console.log("entity Name------------------------------>",entityName);
          let [completeQuery, completeConn] = updateInteraction(
            {
              interactionId,
              type,
              currentStatus,
              statusCompleted,
              garbageEntity,
              endTimeVideoIndexer,
              videoDurationInSeconds,
              detectedMajorSentimentVideo,
              detectedMajorSentimentVideoScore:detectedMajorSentimentVideoScore?detectedMajorSentimentVideoScore:0.5,
              detectedMajorEmotionVideo,
              detectedMajorEmotionVideoScore:detectedMajorEmotionVideoScore?detectedMajorEmotionVideoScore:0.5,
              transcriptText,
              keywords,
              labels,
              brands,
              scenes,
              topics,
              audioEffects,
              videoStatistics,
              language,
              sourceLanguage,
              detectedOrgId,
              entityName,
              conclusiveSentiment,
              conclusiveSentimentScore:totalSentiments?conclusiveSentimentScore/totalSentiments:conclusiveSentimentScore/1
            },
            connection
          )
          let responseData = await completeConn.query(completeQuery);

          // let storedProcedure = ["happinessSummaryInsertion","vipInsertion","insertAspectsAndOpinions","emotionInsertion","languageWiseSentimentInsertion"];
          let storedProcedure = _.get(config, 'sp_digitalAjman', []);
          
          setTimeout(async ()=>{
            storedProcedure.forEach(async sp=>{
              let spQuery = `EXEC ${sp} '${interactionId}','API'`;
              console.log("SP Query----------------------->",spQuery);
              if(sp=="vipInsertion" && (moment().format("YYYY-MM-DD")==moment(submissionTime).format("YYYY-MM-DD")) ){
                console.log("calling vipInsertion");
                try{
                  let spResponse = await connection.query(spQuery);
                  console.log(`${sp} Response------------------------------>`,spResponse);
                }
                catch(spError){
                  console.log(`${sp} Eror-------------------->`,spError);
                  connection.thisConnPool.close()
                }
              }
              if(sp!="vipInsertion"){
                try{
                  let spResponse = await connection.query(spQuery);
                  console.log(`${sp} Response------------------------------>`,spResponse);
                }
                catch(spError){
                  console.log(`${sp} Eror-------------------->`,spError);
                  connection.thisConnPool.close()
                }
              }
              
            })
        
            let procedureQuery = `UPDATE digital_ajman.dbo.interaction SET isProcessed='${1}',jobRunning='${0} WHERE interactionId='${interactionId}'`;
            try{
              let procedureResponse = await connection.query(procedureQuery);
              console.log(`Procedure Update Response------------------------------>`,procedureResponse);
              connection.thisConnPool.close()
              return { id: interactionId, success: true }
            }
            catch(pError){
              console.log(`Procedure update Eror-------------------->`,pError);
              connection.thisConnPool.close()
            }
          },5000)
          

          // connection.thisConnPool.close()
          // return responseData
        }
      }
    } catch (err) {
      console.log('video index service error', err)
      //   indexService(interactionId, videoId)
    }
  }
}

process.on('message', params => {
  global.config = params
  threadStart(params)
  process.send(0)
})
