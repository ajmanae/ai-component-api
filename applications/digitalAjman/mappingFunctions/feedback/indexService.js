'use strict';
const { fork } = require('child_process');

let start = function () {
  const listen = fork('polling.js');
  listen.send(global.config);
  listen.on('message', (err) => {
    if (err) {
      console.log(err);
    }
    console.log('Listen Process Exited for feedback!!');
  });
};

start();
