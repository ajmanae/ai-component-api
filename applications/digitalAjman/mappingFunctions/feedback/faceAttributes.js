'use strict';
const sqlserver = require('../../../../core/api/connectors/mssql');
const rp = require('request-promise');
const moment = require('moment');
const saveFile = require('./saveFile');
// const textIndexer = require("./textIndexer");

async function faceAttributes(payload, UUIDKey, route, callback, JWToken) {
    console.log('payload----------------------->', payload);
    // const thumbnailPath = await saveFile(payload,UUIDKey,JWToken);
    // console.log("thumbnail path-------------------->",thumbnailPath);
    let key = 'eb3fd59d473047b6a6af8fffc49040e9';
    let uri= 'https://ai-customer-happiness-face.cognitiveservices.azure.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&recognitionModel=recognition_03&returnRecognitionModel=false&detectionModel=detection_01&returnFaceAttributes=age,gender,emotion';
    
    let file = payload.files.file;
    var r = rp({
        method: 'POST',
        uri: uri,
        headers: {
            'Content-Type': 'application/octet-stream',
            'Ocp-Apim-Subscription-Key': key,
        },
        json: true // Because i want json response
    });
    r.body = file.data; // Put the file here
    r.then(function (response) {
        console.log("face attr >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",JSON.stringify(response))
        // return callback({
        //     faceResponse:{
        //         "faceId":"22e5c2b3-275c-41d6-8ee5-d7d8b6ab6cfb",
        //         "faceRectangle":{"top":318,"left":206,"width":171,"height":171},
        //         "faceAttributes":{
        //             "gender":"female",
        //             "age":29,
        //             "emotion":{
        //                 "anger":0.703,
        //                 "contempt":0.04,
        //                 "disgust":0.038,
        //                 "fear":0.098,
        //                 "happiness":0.015,
        //                 "neutral":0.027,
        //                 "sadness":0.008,
        //                 "surprise":0.07
        //             }
        //         }
        //     },
        //     success:true
        // })
        return callback({faceResponse:response.length?response[0]:response,success:true});
    }).catch(function (error) {
        console.log(err,"face api error>>>>>>>>>>>>",err);
        return callback({err,success:false});
    });

}

exports.faceAttributes = faceAttributes;
