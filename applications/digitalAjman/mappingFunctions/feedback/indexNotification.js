'use strict';
const sqlserver = require('../../../../core/api/connectors/mssql');
const rp = require('request-promise');
const moment = require('moment');

async function indexNotification(payload, UUIDKey, route, callback, JWToken) {
    console.log('Index Notification----------------------->', payload);
    return callback({success: false })
}

exports.indexNotification = indexNotification;
