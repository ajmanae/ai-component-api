'use strict'
const rp = require('request-promise');

module.exports = async function languageIndexer(textData) {
   console.log("text payload-------------------------------->",textData);
   try {
    let key = '3f7517d66d8849a3afb05fb0082fed6b';
    let uri= 'https://ai-customer-happiness-textanalytics.cognitiveservices.azure.com/text/analytics/v3.0/languages';

    //uplaoding text

    var textOptions = {
        method: 'POST',
        uri: uri,
        body: textData,
        headers: {
           'Content-Type': 'application/json',
           'Ocp-Apim-Subscription-Key': key,
        },
        json:true
    };

    let textResponse = await rp(textOptions);
    console.log("text indexer Response------------>",textResponse);
    let detectedLanguages = {};
    textResponse.documents.forEach(document => {
        if(detectedLanguages[document.detectedLanguage.name]){
            detectedLanguages[document.detectedLanguage.name] += document.detectedLanguage.confidenceScore;
        }else{
            detectedLanguages[document.detectedLanguage.name] = document.detectedLanguage.confidenceScore;
        }
    });

    let detectedLanguage;
    let confidence=0;

    Object.keys(detectedLanguages).map(o=>{
        if(detectedLanguages[o]>confidence){
            confidence = detectedLanguages[o];
            detectedLanguage=o;
        }
    });
    console.log("detected language------------------>",detectedLanguage);

    return detectedLanguage;
    
   } catch (err) {
       console.log("language error--------------------->",err)
      return null;
   }
}

// languageIndexer({
//     "documents": [
//       {
//                     "id":1,
//                     "text":"Hello, this is from hard uploading the video triple.",
//                  },
//                   {
//         "id": "2",
//         "text": "The restaurant had great food and our waiter was friendly."
//       }
//     ]
//   });

