'use strict'
const rp = require('request-promise')
const fs = require('fs')
const path = require('path')
const config = require('../../../../config/index')

module.exports = async function videoIndexer (payload, language, completePath) {
  console.log('video payload-------------------------------->', payload)
  try {
    let accountId = '18c0e61d-d611-4526-86f7-ffe0f75fe9d5'
    let location = 'eastus'
    let key = '72c126cececc496a909566afb5f36441'
    let personModelId = '96dc877f-4f5a-47df-adad-533e4cea77c7'
    let uri = 'https://api.videoindexer.ai'
    let getTokenUri = `${uri}/Auth/${location}/Accounts?generateAccessTokens=true&allowEdit=true`
    var getTokenOptions = {
      method: 'GET',
      uri: getTokenUri,
      timeout: 200000,
      headers: {
        'Ocp-Apim-Subscription-Key': key
      },
      json: true
    }

    //get access token
    let tokenResponse = await rp(getTokenOptions)
    console.log(tokenResponse, 'Token Response------------------------------->')
    let accessToken = tokenResponse[tokenResponse.length - 1].accessToken
    console.log(completePath, 'complete Path------------------->')

    //uplaoding video
    let videoUploadUri = `${uri}/${location}/Accounts/${accountId}/Videos?name=${payload.interactionId}&externalId=${payload.interactionId}&language=${language}&personModelId=${personModelId}&priority=High&accessToken=${accessToken}`
    console.log("Video upload URI>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",videoUploadUri);
    // let videoUploadUri = `${uri}/${location}/Accounts/${accountId}/Videos?name=${payload.interactionId}&externalId=${payload.interactionId}&personModelId=${personModelId}&priority=High&accessToken=${accessToken}`
    // let file = fs.readFileSync(completePath)
    let file = payload.files.file;
    var videoOptions = {
      method: 'POST',
      uri: videoUploadUri,
      timeout: 200000,
      formData: {
        file: {
          value: file.data,
          options: {
            filename: file.name,
            contentType: file.mimetype
          }
        }
      },
      headers: {
        'Content-Type': 'multipart/form-data',
        'Ocp-Apim-Subscription-Key': key
      },
      json: true
    }
    let videoResponse = await rp(videoOptions)
    return videoResponse;
  } catch (err) {
    console.log('UPLOADED bad error ->>>>>>>>>>>>>>>', err)
    return { ...err, errorFlag: true }
  }
}
