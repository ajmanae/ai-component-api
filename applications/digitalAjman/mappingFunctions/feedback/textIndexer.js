'use strict'
const sqlserver = require('../../../../core/api/connectors/mssql');
const rp = require('request-promise');

module.exports = async function upload(textData) {
   console.log("text payload-------------------------------->",textData);
   try {
    let key = '3f7517d66d8849a3afb05fb0082fed6b';
    let uri= 'https://ai-customer-happiness-textanalytics.cognitiveservices.azure.com/text/analytics/v3.1-preview.1/sentiment?opinionMining=true';

    //uplaoding text

    var textOptions = {
        method: 'POST',
        uri: uri,
        body: textData,
        headers: {
           'Content-Type': 'application/json',
           'Ocp-Apim-Subscription-Key': key,
        },
        json:true
    };

    let textResponse = await rp(textOptions);

    console.log("text indexer Response------------>",textResponse);

    return textResponse;
    
   } catch (err) {
      return err;
   }
}
