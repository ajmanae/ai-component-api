'use strict';
const sqlserver = require('../../../../core/api/connectors/mssql');
const rp = require('request-promise');
const moment = require('moment');
const languageIndexer = require("./languageIndexer");
const keyphraseIndexer = require("./keyphraseIndexer");
const NERIndexer = require("./nerIndexer");
const textIndexer = require("./textIndexer");
const personIndexing = require("./personIndexing");
// const indexService = require('./indexService');
const {
    insertInteraction,
    updateInteraction,
} = require('../../lib/queries.js');
const { lang } = require('moment');

async function reindexInteraction(payload, UUIDKey, route, callback, JWToken) {
    let interactionId = payload.interactionId;
    let language = payload.language;
    let accountId = '18c0e61d-d611-4526-86f7-ffe0f75fe9d5';
    let location = 'eastus';
    let personModelId = '96dc877f-4f5a-47df-adad-533e4cea77c7'
    let key = '72c126cececc496a909566afb5f36441';
    let uri= 'https://api.videoindexer.ai';
    let getTokenUri=`${uri}/Auth/${location}/Accounts?generateAccessTokens=true&allowEdit=true`;
    var getTokenOptions = {
        method: 'GET',
        uri: getTokenUri,
        headers: {
        'Ocp-Apim-Subscription-Key': key
        },
        json:true
    };
    let accessToken;

    try{
         //get access token
         let tokenResponse = await rp(getTokenOptions);
         console.log(tokenResponse,"Token Response------------------------------->");
         accessToken = tokenResponse[tokenResponse.length-1].accessToken;
    }catch(err){
        console.log(err,"Error in getting token");
        callback({err,success:true})
    }

    let connection = await sqlserver.connection();
    let query = `SELECT videoIndexerUploadResponse,type,sourceLanguage FROM digital_ajman.dbo.interaction WHERE interactionId='${interactionId}'`;
    let queryResponse = await connection.query(query);
    console.log(queryResponse,"query response----------------->")
    let videoResponse = JSON.parse(queryResponse.recordset[0].videoIndexerUploadResponse);
    console.log(videoResponse,"video response----------->")
    // let type = queryResponse.recordset[0].type;
    // let sourceLanguage = queryResponse.recordset[0].sourceLanguage;
       
        //reindexing video
        let ReIndexUri = `https://api.videoindexer.ai/${location}/Accounts/${accountId}/Videos/${videoResponse.id}/ReIndex?personModelId=${personModelId}&priority=High&accessToken=${accessToken}&sourceLanguage=${language}`;
        console.log(ReIndexUri,"ReIndex URI------------------------------------>")

        var IndexOptions = {
            method: 'PUT',
            uri: ReIndexUri,
            headers: {
            'Ocp-Apim-Subscription-Key': key,
            },
            json:true
        };

        let IndexResponse;
        try{
            videoResponse.sourceLanguage = language;
            IndexResponse = await rp(IndexOptions);
            let query1 = `UPDATE digital_ajman.dbo.interaction 
            SET 
            currentStatus='${"RECEIVED"}',detectedPersonID='${""}',convertedText='${""}',transcriptText='${""}',jobRunning='0',videoIndexerUploadResponse='${JSON.stringify(videoResponse)}' WHERE interactionId='${interactionId}'`;
            console.log("REINDEX QUERY-------------------->",query1)
            let queryResponse1 = await connection.query(query1);
            console.log(queryResponse1,"query1 response----------------->")
            connection.thisConnPool.close();
            callback({status:"REINDEXED",success:true})
        }
        catch(err){
            console.log(err,"errrrrrrrrrrrrrrrrrr----------------->")
            callback({status:"Not Found",success:false})
        }
   

}

exports.reindexInteraction = reindexInteraction;
