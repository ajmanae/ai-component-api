'use strict'
const rp = require('request-promise')
const languageConverter = require('./languageConvertor')
const uuid = require('uuid/v1')
const Fdkaac = require('node-fdkaac').Fdkaac
const fs = require('fs')

module.exports = async function speechIndexer (payload, language, filePath) {
  // async function speechIndexer(textData,from) {
  console.log(
    'conversion payload-------------------------------->',
    payload,
    language
  )

  let location = 'centralindia'
  let fName = uuid()
  let key = '38230a860b124084a579860ad677d765'
  let uri = `https://${location}.stt.speech.microsoft.com/speech/recognition/conversation/cognitiveservices/v1?language=${language}`
  let file = payload.files.file
  await file.mv(`./upload/${fName}.aac`);
    // console.log("FDAA source File Path------------------->",filePath);
  const decoder = new Fdkaac({
    output: `./upload/${fName}.wav`
  }).setFile(`./upload/${fName}.aac`)

  await decoder.decode()

  var r = rp({
    method: 'POST',
    uri: uri,
    headers: {
      'Content-Type': 'audio/wave',
      'Ocp-Apim-Subscription-Key': key
    },
    json: true // Because i want json response
  })

  r.body = fs.readFileSync(`./upload/${fName}.wav`)

  const speechdata = () => {
    return new Promise((resolve, reject) => {
      r.then(function (speechResponse) {
        resolve(speechResponse)
      }).catch(function (err) {
        return reject(err)
      })
    })
  }

  try {
    let speechResponse = await speechdata()
    console.log(speechResponse, 'Speech Response----------------->')
    let textData = await languageConverter(
      [
        {
          id: 1,
          text: speechResponse.DisplayText
        }
      ],
      language
    )
    console.log('converted Data-------------------->', textData)
    return textData
  } catch (err) {
    console.log(err, 'speechindexer error')
    return null
  }
}

// speechIndexer([
//     {
//         "id": 1,
//         "text": "السلام عليكم ورحمة الله، قمت بتقديم على خدمة في غرفة عجمان."
//     },
//     {
//         "id": 2,
//         "text": "آه إجراءات. ها كانت جدا طويلة ومعقدة."
//     },
//     {
//         "id": 3,
//         "text": "أتمنى إعادة النظر في هذه الإجراءات."
//     }
// ],"ar-EG");
