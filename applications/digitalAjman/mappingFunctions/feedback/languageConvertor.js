'use strict'
const rp = require('request-promise');

module.exports = async function languageConvertor(textData,from) {
    // async function languageConvertor(textData,from) {
   console.log("conversion payload-------------------------------->",textData,from);
   try {
    let key = 'a92e7b0f1b9147a0bafbf139c1fb0c4e';
    let location = "centralindia";
    let uri= `https://api.cognitive.microsofttranslator.com/translate?api-version=3.0&from=${from}&to=en`;

    //uplaoding text

    var convertorOptions = {
        method: 'POST',
        uri: uri,
        body: textData,
        headers: {
           'Content-Type': 'application/json',
           'Ocp-Apim-Subscription-Key': key,
           'Ocp-Apim-Subscription-Region':location
        },
        json:true
    };

    let convertorResponse = await rp(convertorOptions);

    console.log("convertor Response------------>",convertorResponse);

    let translations=[];

    convertorResponse.forEach(document => {
        translations=[...translations,...document.translations]
    });

    // console.log("translations--------------------->",translations.map((t,i)=>{
    //     return {
    //         id:i+1,
    //         text:t.text
    //     }
    // }));

    return translations.map((t,i)=>{
        return {
            id:i+1,
            text:t.text
        }
    });
    
   } catch (err) {
       console.log("conversion err----------------->",err);
      return null;
   }
}


// languageConvertor([
//     {
//         "id": 1,
//         "text": "السلام عليكم ورحمة الله، قمت بتقديم على خدمة في غرفة عجمان."
//     },
//     {
//         "id": 2,
//         "text": "آه إجراءات. ها كانت جدا طويلة ومعقدة."
//     },
//     {
//         "id": 3,
//         "text": "أتمنى إعادة النظر في هذه الإجراءات."
//     }
// ],"ar-EG");

