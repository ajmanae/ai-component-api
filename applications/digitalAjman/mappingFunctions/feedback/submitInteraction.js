'use strict';
const sqlserver = require('../../../../core/api/connectors/mssql');
const rp = require('request-promise');
const moment = require('moment');
const {
    insertInteraction,
    updateInteraction,
} = require('../../lib/queries.js');
// const textIndexer = require("./textIndexer");

async function submitInteraction(payload, UUIDKey, route, callback, JWToken) {
    console.log('payload----------------------->', payload);
        let interactionId = payload.interactionId;
        let customerProvidedSentiment = payload.customerProvidedSentiment;
        let detectedMajorSentimentFace =  payload["emotion"]?payload.emotion:null;
        let detectedMajorSentimentFaceScore = payload["confidence"]?payload.confidence:0;
        let age = payload["age"]?payload.age:0;
        let gender = payload["gender"]?payload.gender:null;
        let nationality = payload["nationality"]?payload.nationality:null;
        let type = payload.type;
        let connection = await sqlserver.connection();
        let currentStatus="SUBMIT"

        try {
            let [submitQuery , submitConn] = updateInteraction({
                interactionId,
                currentStatus,
                customerProvidedSentiment,
                age,
                gender,
                nationality,
                detectedMajorSentimentFace,
                detectedMajorSentimentFaceScore,
            }, connection);
            let responseData2 = await submitConn.query(submitQuery);
            console.log(responseData2,"Submitted interaction-------------------->")
            connection.thisConnPool.close();
            return callback({responseData2,success:true});
        } catch (err) {
            console.log(err,"interaction update error>>>>>>>>>>>>",err);
            connection.thisConnPool.close();
            return callback({err,success:false});
        }

}

exports.submitInteraction = submitInteraction;
