'use strict'
const sqlserver = require('../../../../core/api/connectors/mssql')
const rp = require('request-promise')
const moment = require('moment')
const uuid = require('uuid/v1')
const fs = require('fs')
const FileType = require('file-type')
const { insertInteraction, updateInteraction } = require('../../lib/queries.js')
const saveFile = require('./saveFile')
const _ = require("lodash")

module.exports = async function personIndexing (faces, interactionId) {
  // async function personIndexing(faces,interactionId) {

  console.log('faces payload-------------------------------->', faces)
  //   console.log(JWToken, 'JWTToken---------------->')
  let accountId = '18c0e61d-d611-4526-86f7-ffe0f75fe9d5'
  let location = 'eastus'
  let key = '72c126cececc496a909566afb5f36441'
  let uri = 'https://api.videoindexer.ai'
  let faceKey = 'cb463c6d4e3e4cc6b385050344186494'
  let faceUri =
    'https://uaenorth.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&recognitionModel=recognition_03&returnRecognitionModel=false&detectionModel=detection_01&returnFaceAttributes=age,gender,emotion'
  let getTokenUri = `${uri}/Auth/${location}/Accounts?generateAccessTokens=true&allowEdit=true`
  let getTokenOptions = {
    method: 'GET',
    uri: getTokenUri,
    headers: {
      'Ocp-Apim-Subscription-Key': key
    },
    json: true
  }
  try {
    let detectedPersonID;
    let connection = await sqlserver.connection()

    faces.forEach(async face => {
      //get access token
      let tokenResponse = await rp(getTokenOptions)
      console.log(
        tokenResponse,
        'Token Response------------------------------->'
      )
      let accessToken = tokenResponse[tokenResponse.length-1].accessToken

      let thumbnailId = face.thumbnailId
      let videoId = face.videoId
      let name = face.name
      let confidence = face.confidence * 100
      let personId = `P-${new Date().getTime()}`

      //thumbnail uri
      let thumbnailUri = `https://api.videoindexer.ai/${location}/Accounts/${accountId}/Videos/${videoId}/Thumbnails/${thumbnailId}/?accessToken=${accessToken}`

      console.log('thumbnail URL--------------------->', thumbnailUri)

      let thumbnailOptions = {
        method: 'GET',
        uri: thumbnailUri,
        encoding: null,
        headers: {
          'Content-Type': 'application/json',
          'Ocp-Apim-Subscription-Key': key
        },
        json: true // Because i want json response
      }

      let file = await rp(thumbnailOptions)
      const buffer = Buffer.from(file)
      const bufferInfo = await FileType.fromBuffer(buffer)
      const {filePath,downloadPath} = await saveFile(
        {
          files: {
            file: {
              data: buffer,
              name: `${personId}.${bufferInfo.ext}`,
              mimetype: `image/${bufferInfo.ext}`
            }
          }
        },
        uuid()
      )

      let faceOptions = {
        method: 'POST',
        uri: faceUri,
        body: {
          url: thumbnailUri
        },
        headers: {
          'Content-Type': 'application/json',
          'Ocp-Apim-Subscription-Key': faceKey
        },
        json: true // Because i want json response
      }

      let faceResponse = await rp(faceOptions)
      console.log(faceResponse, 'Face Response -------------------------->')
      if (faceResponse.length) {
        detectedPersonID = personId;
        let isVIP = (_.includes(face.name, 'Unknown')) || (confidence < 90)?0:1;
        let faceEmotionRecognition = JSON.stringify(faceResponse)
        let faceAttributes = faceResponse[0].faceAttributes
        let gender = faceAttributes.gender
        let age = faceAttributes.age
        let majorEmotionFace = null
        let majorEmotionFaceScore = 0;
        let base = "https://customer-happiness.uaenorth.cloudapp.azure.com/api";
        Object.keys(faceAttributes.emotion).map(o => {
          if (faceAttributes.emotion[o] * 100 > majorEmotionFaceScore) {
            majorEmotionFaceScore = faceAttributes.emotion[o] * 100
            majorEmotionFace = o
          }
        })

        let query1 = `INSERT INTO digital_ajman.dbo.person 
            (personId,isVIP,name,videoId,confidence,interactionId,thumbnailId,faceEmotionRecognition,gender,age,majorEmotionFace,majorEmotionFaceScore,thumbnailPath,base) 
            VALUES ('${personId}','${isVIP}','${name}','${videoId}','${confidence}','${interactionId}','${thumbnailId}','${faceEmotionRecognition}','${gender}','${age}','${majorEmotionFace}','${majorEmotionFaceScore}','${downloadPath}','${base}')`
        console.log('query=======================', query1)
        let responseData1 = await connection.query(query1)
        console.log(
          responseData1,
          'face responseData1----------------------------->'
        )

        console.log('detected Persons-------->', detectedPersonID)
        let currentStatus = 'PERSON'
        let [PersonQuery, PersonConn] = updateInteraction(
          {
            interactionId,
            currentStatus,
            detectedPersonID,
            isVIPPerson:isVIP,
            detectedMajorEmotionFace:majorEmotionFace,
            detectedMajorEmotionFaceScore:majorEmotionFaceScore,
            age,
            gender
          },
          connection
        )
        let responseData2 = await PersonConn.query(PersonQuery)
        console.log(
          responseData2,
          'response data people ids--------------------->'
        )
      }
    })
  } catch (err) {
    console.log('person indexing error------------------->', err)
    return err
  }
}
