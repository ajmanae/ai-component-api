'use strict';
const rp = require('request-promise');
const fs = require('fs');


// module.exports = async function automatedScript() {
async function automatedScript() {
    let interactionId;
    let orgId="DDA";
    let language="hi-IN";
    let type="V";
    let customerProvidedSentiment="neutral";
    const clientKey = '3d44a382820ca884b74110fc1eebe5984f524343f78e58bd5958ea56fbf195907203c6b4b99fc40d6c5d18ec12bf41f9bde3aa84a47bf6a7b1bbb107c07937a8';
    const clientSecret = '70747a4ad98badf11465ae80f45e047a1832f1805ebe91b473295d92500a922dd9b6b41c91ebd34beb0f692b40a303a27a6910f5075c6b6897da639e85335463';
    
    for(let i=23;i<=23;i++){
    const path = `C:/Users/Avanza/Downloads/Videos/${i}.mp4`;
    const name = `${i}.mp4`;


    let quickform = {
        'file': {
          'value': fs.createReadStream(type=="A"?path:'C:/Users/Avanza/Downloads/response.jpeg'),
          'options': {
            'filename': type=="A"?name:'response.jpeg',
            'contentType': type=="A"?'audio/mp3':'video/mp4'
          }
        },
        'type': `${type}`,
        'language': `${language}`
      }

    const quickOptions = {
        method: "POST",
        uri: "https://customer-happiness.uaenorth.cloudapp.azure.com/api/PUBLIC/Feedback/quickFeedback",
        headers: {
            'Content-Type': 'multipart/form-data',
            'clientKey':clientKey,
            'clientSecret':clientSecret
        },
        formData: quickform,
        json:true
    };
    
    let quickResponse;
    try{
    quickResponse= await rp(quickOptions);
    console.log("quick Response------------>",quickResponse);
    }catch(err){
        console.log(err,"quick response error");
    }

    interactionId = quickResponse.interactionId;

    let interactionform = {
        'name': `${name}`,
        'file': {
          'value': fs.createReadStream(path),
          'options': {
            'filename': name,
            'contentType': type=="A"?'audio/mp3':'video/mp4'
          }
        },
        'type': `${type}`,
        'orgId': `${orgId}`,
        'interactionId': `${interactionId}`,
        'language': `${language}`
    }

    const feedbackOptions = {
        method: "POST",
        uri: "https://customer-happiness.uaenorth.cloudapp.azure.com/api/PUBLIC/Feedback/uploadInteraction",
        headers: {
            'Content-Type': 'multipart/form-data',
            'clientKey':clientKey,
            'clientSecret':clientSecret,
        },
        formData: interactionform,
        json:true
    };

    let feedbackResponse;
    try{
        feedbackResponse= await rp(feedbackOptions);
        console.log("feedback Response------------>",interactionform);
    }catch(err){
        console.log(err,"feedback response error");
    }

    const submitOptions = {
        method: "POST",
        uri: "https://customer-happiness.uaenorth.cloudapp.azure.com/api/PUBLIC/Feedback/submitInteraction",
        headers: {
            'Content-Type': 'application/json',
            'clientKey':clientKey,
            'clientSecret':clientSecret
        },
        body: {
            type:`${type}`,
            interactionId:`${interactionId}`,
            customerProvidedSentiment:`${customerProvidedSentiment}`
        },
        json:true
    };

    let submitResponse;
    try{
        submitResponse= await rp(submitOptions);
        console.log("submit Response------------>",submitResponse);
    }catch(err){
        console.log(err,"submit response error");
    }
}
}

automatedScript();