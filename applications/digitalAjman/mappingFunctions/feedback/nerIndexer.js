'use strict'
const sqlserver = require('../../../../core/api/connectors/mssql');
const rp = require('request-promise');

module.exports = async function nerIndexer(textData) {
   console.log("text payload-------------------------------->",textData);
   try {
    let key = '3f7517d66d8849a3afb05fb0082fed6b';
    let uri= 'https://ai-customer-happiness-textanalytics.cognitiveservices.azure.com/text/analytics/v3.1-preview.2/entities/recognition/general';

    //uplaoding text

    var textOptions = {
        method: 'POST',
        uri: uri,
        body: textData,
        headers: {
           'Content-Type': 'application/json',
           'Ocp-Apim-Subscription-Key': key,
        },
        json:true
    };

    let textResponse = await rp(textOptions);

    console.log("text indexer Response------------>",textResponse);

    let entities=[];

    textResponse.documents.forEach(document => {
        entities=[...entities,...document.entities]
    });

    console.log("entities--------------------->",entities);

    return entities;
    
   } catch (err) {
      console.log(err,"NER error----------------------->")
      return null;
   }
}


// nerIndexer({
//     "documents": [
//       {
//                     "id":1,
//                     "text":"Hello, this is from hard uploading the video triple.",
//                  },
//                   {
//         "id": "2",
//         "text": "The restaurant had great food and our waiter was friendly."
//       }
//     ]
//   });

