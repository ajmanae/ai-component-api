'use strict';
const sqlserver = require('../../../../core/api/connectors/mssql');
let sql = require('mssql');
const rp = require('request-promise');
const moment = require('moment');
const languageIndexer = require("./languageIndexer");
const keyphraseIndexer = require("./keyphraseIndexer");
const NERIndexer = require("./nerIndexer");
const textIndexer = require("./textIndexer");
const personIndexing = require("./personIndexing");
// const indexService = require('./indexService');
const {
    insertInteraction,
    updateInteraction,
} = require('../../lib/queries.js');

async function indexInteraction(payload, UUIDKey, route, callback, JWToken) {
    let interactionId = payload.interactionId;
    let convertedText = payload["convertedText"]?payload.convertedText:null;
    let providedText = payload["providedText"]?payload.providedText:null;
    let transcriptText = payload["transcriptText"]?payload.transcriptText:null;
    try{
        let connection = await sqlserver.connection();
        connection.input('transcriptText', sql.NVarChar,  JSON.stringify(transcriptText));
        connection.input('convertedText', sql.NVarChar,  transcriptText?"":JSON.stringify(convertedText))
        connection.input('providedText', sql.NVarChar, providedText);
        connection.input('interactionId', sql.VarChar, interactionId);
        let query = `UPDATE digital_ajman.dbo.interaction 
        SET 
        currentStatus='${convertedText||transcriptText?"ANALYZING KEYPHRASES":"RECEIVED"}',jobRunning='0',detectedPersonID='${""}'${convertedText||transcriptText?`,convertedText=@convertedText`:""}${providedText?`,providedText=@providedText`:""}${transcriptText?`,transcriptText=@transcriptText`:""} WHERE interactionId=@interactionId`;
        console.log("query------------------------>",query);
        let queryResponse = await connection.query(query);
        console.log(queryResponse,"query response----------------->")
        connection.thisConnPool.close();
        callback({status:"INDEXED",success:true})
    }catch(err){
        console.log("indexing interaction error-------------------->",err);
        callback({status:"NOT FOUND",success:false})
    }
    
}

exports.indexInteraction = indexInteraction;
