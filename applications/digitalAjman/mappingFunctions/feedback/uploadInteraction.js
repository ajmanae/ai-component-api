'use strict'
const sqlserver = require('../../../../core/api/connectors/mssql')
const rp = require('request-promise')
const moment = require('moment')
const languageIndexer = require('./languageIndexer')
const keyphraseIndexer = require('./keyphraseIndexer')
const NERIndexer = require('./nerIndexer')
const textIndexer = require('./textIndexer')
const videoIndexer = require('./videoIndexer')
const speechIndexer = require('./speechIndexer')
const languageConverter = require('./languageConvertor')
const config = require('../../../../config')
const path = require('path')
const fs = require('fs')
// const indexService = require('./indexService')
const { insertInteraction, updateInteraction } = require('../../lib/queries.js')
const saveFile = require('./saveFile')
const uuid = require('uuid/v1')
const Fdkaac = require('node-fdkaac').Fdkaac
const _ = require('lodash')

const languageMapping = {
  Arabic: 'ar-EG',
  English: 'en-US'
}

// const keywords={
//   "Japan":"Ajman", 
//   "Dutchmen":"Ajman",
//   "Edge Man":"Ajman",
//   "Magic Man":"Ajman",
//   "German":"Ajman",
//   "Judgement":"Ajman",
//   "Man Free Zone":"Ajman Free Zone",
//   "Ajman Ember":"Ajman Chamber",
//   "advent chamber":"Ajman Chamber",
//   "General Police":"Ajman Police",
//   "When Departmeny":"Ajman Department",
//   "Village Man":"Digital Ajman",
//   "Josh Goardan Holiday":"Transport Authority",
//   "Batman Protest":"Ajman Police",
//   "customer services":"Custome Services",
//   "Number":"Ajman Chamber",
//   "Board and customs":"Port and Customs",
//   "Department of lying":"Department of Land"
// }

// function getOrgsData () {
//   return new Promise(resolve => {
//     global.db.select('Entity', {}, '', function (err, orgsData) {
//       if (err) {
//         resolve(undefined)
//       } else {
//         resolve(orgsData)
//       }
//     })
//   })
// }

async function uploadInteraction (payload, UUIDKey, route, callback, JWToken) {
  console.log('payload----------------------->', payload)
  console.log(
    'JWTTTTT Org Code--------------------->',
    _.get(JWToken, 'orgCode', '')
  )
  let type = payload.type
  let submissionTime = moment().format('YYYY-MM-DD HH:mm:ss')
  let interactionId = payload['interactionId']
    ? payload.interactionId
    : `${type}-${uuid()}`
  let orgId = _.get(JWToken, 'orgCode', '')
  let language = payload['language'] ? payload.language : 'en-US'
  if (type == 'V' || type == 'A') {
    console.log('EXEC started!')

    let currentStatus = 'RECEIVED'
    let statusCompleted = []
    var { filePath, downloadPath } = await saveFile(payload, UUIDKey, JWToken)
    console.log(filePath)
   
    setImmediate(async (filePath) => {
      let videoResponse = await videoIndexer({...payload,interactionId}, language, filePath)
      if (videoResponse && videoResponse.errorFlag) {
        throw 'Ex From Video Indexer'
      }
      // let startTimeLanguageConversion = moment().format('YYYY-MM-DD HH:mm:ss')
      // let convertedText = []
      // // if (type == 'A') {
      // convertedText = await speechIndexer(payload, language, filePath)
      // console.log('converted Text-------------->', convertedText);

      // if(convertedText){
      //   Object.keys(keywords).forEach(keyword=>{
      //     let re = new RegExp(keyword,"gi");
      //     convertedText[0].text = convertedText[0].text.replace(re, keywords[keyword]) 
      //   })
      // }

      // console.log('Altered converted Text-------------->', convertedText);
      // let endTimeLanguageConversion = moment().format('YYYY-MM-DD HH:mm:ss')

      let connection = await sqlserver.connection()
      let videoindexerUploadResponse = videoResponse
      let [insetQuery, insertConn] = insertInteraction(
        {
          interactionId,
          type,
          orgId,
          filePath: downloadPath,
          currentStatus,
          submissionTime,
          // startTimeLanguageConversion,
          // endTimeLanguageConversion,
          // convertedText: convertedText ? convertedText : [],
          videoindexerUploadResponse
        },
        connection
      )

      try {
        await insertConn.query(insetQuery)
        currentStatus = type == 'V' ? 'ANALYZING VIDEO' : 'ANALYZING AUDIO'
        statusCompleted.push('RECEIVED')
        let startTimeVideoIndexer = moment().format('YYYY-MM-DD HH:mm:ss')
        let [updateQuery, updateConn] = updateInteraction(
          {
            interactionId,
            currentStatus,
            statusCompleted,
            startTimeVideoIndexer
          },
          connection
        )
        await updateConn.query(updateQuery)
        console.log('EXEC completed!')
        return
      } catch (err) {
        console.log(err, 'text uploading error>>>>>>>>>>>>', err)
        return callback({ err, success: false })
      }
    })
    callback({ id: interactionId, success: true })
    console.log('file uploaded to server!!!, Starting processing!!!!')
  } else {
    let connection = await sqlserver.connection()
    let responseData

    let garbageEntity = 0
    let providedText = payload.textData.documents[0].text
    let currentStatus = 'RECEIVED'
    let [receivedQuery, receivedConn] = insertInteraction(
      {
        interactionId,
        type,
        garbageEntity,
        orgId,
        currentStatus,
        submissionTime,
        providedText
      },
      connection
    )

    try {
      responseData = await receivedConn.query(receivedQuery)
      return callback({ interactionId, success: false })
    } catch (err) {
      console.log(err, 'DETECTING LANGUAGE error>>>>>>>>>>>>', err)
      return callback({ err, success: false })
    }
  }
}

exports.uploadInteraction = uploadInteraction
