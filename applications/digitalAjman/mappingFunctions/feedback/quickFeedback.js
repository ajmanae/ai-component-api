'use strict'
const sqlserver = require('../../../../core/api/connectors/mssql')
const rp = require('request-promise')
const fs = require('fs')
const moment = require('moment')
const textIndexer = require('./textIndexer')
const languageConverter = require('./languageConvertor')
const speechIndexer = require('./speechIndexer')
const languageIndexer = require('./languageIndexer')
const uuid = require('uuid/v1')
const Fdkaac = require('node-fdkaac').Fdkaac;
const saveFile = require('./saveFile');
const { response } = require('express')

const languageMapping ={
  Arabic: 'ar-EG',
  English: 'en-US',
  Hindi:'hi-IN',
  Urdu:'ur-PK'
}

async function quickFeedback (payload, UUIDKey, route, callback, JWToken) {
  console.log('payload----------------------->', payload)
  let type = payload.type
  let language = payload.language
  let interactionId = `${type}-${uuid()}`
  let connection = await sqlserver.connection()
  let query = `INSERT INTO digital_ajman.dbo.interaction ( interactionId,currentStatus,submissionTime ) VALUES ('${interactionId}','${'TEMP'}','${moment().format(
    'YYYY-MM-DD HH:mm:ss'
  )}')`
  try {
    let reponse = await connection.query(query)
  } catch (err) {
    console.log(err, 'quick feedback insert error', err)
    connection.thisConnPool.close()
    return callback({ err, success: false })
  }
  if (type == 'A') {
    console.log('quick feedback---------------------->Audio')
    try {
      let speechResponse = await speechIndexer(payload, language)
      let textResponse = await textIndexer({
        documents: speechResponse
      })
      // console.log('Sentiments Response-------------------->', textResponse)
      let detectedMajorSentimentAudio = null
      let detectedMajorSentimentAudioScore = 0
      let AllSentiments1 = {
        neutral: 0,
        positive: 0,
        negative: 0
      }
      textResponse.documents &&
        textResponse.documents.forEach(document => {
          AllSentiments1[
            document.sentiment == 'mixed' ? 'neutral' : document.sentiment
          ] += document.confidenceScores[document.sentiment]
        })

      console.log(AllSentiments1, 'ALL Senitments1-------------->')
      Object.keys(AllSentiments1).map(o => {
        if (AllSentiments1[o] > detectedMajorSentimentAudioScore) {
          detectedMajorSentimentAudioScore = AllSentiments1[o]
          detectedMajorSentimentAudio = o
        }
      })
      return callback({
        detectedMajorSentimentAudio,
        detectedMajorSentimentAudioScore,
        speechToText: speechResponse,
        interactionId,
        success: true
      })
    } catch (err) {
      console.log('quick audio error!!', err)
      return callback({ err, success: false })
    }

  }
  if (type == 'V') {
    console.log('quick feedback---------------------->Video');
    // const thumbnailPath = await saveFile(payload,UUIDKey,JWToken);
    // console.log("thumbnail path-------------------->",thumbnailPath);
    let key = 'cb463c6d4e3e4cc6b385050344186494'
    let uri =
      'https://uaenorth.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&recognitionModel=recognition_03&returnRecognitionModel=false&detectionModel=detection_01&returnFaceAttributes=age,gender,emotion'

    let file = payload.files.file
    var r = rp({
      method: 'POST',
      uri: uri,
      headers: {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': key
      },
      json: true // Because i want json response
    })
    r.body = file.data // Put the file here
    r.then(function (responses) {
      console.log('FACE API Responses--------------------->', JSON.stringify(responses))
      if(!responses.length){
        return callback({
          interactionId,
          success: true
        })
      }
      let emotion = {
        anger: 0,
        contempt: 0,
        disgust: 0,
        fear: 0,
        happiness: 0,
        neutral: 0,
        sadness: 0,
        surprise: 0
      }
      let confidence = 0
      let age = 0
      let gender = null
      let majorEmotion = null

      responses.forEach(response => {
        gender = response.faceAttributes.gender
        age += response.faceAttributes.age
        Object.keys(response.faceAttributes.emotion).map(o => {
          emotion[o] += response.faceAttributes.emotion[o]
        })
      })

      Object.keys(emotion).map(o => {
        if (emotion[o] / responses.length > confidence) {
          confidence = emotion[o] / responses.length
          majorEmotion = o
        }
      })

      age = age / responses.length

      return callback({
        interactionId,
        age,
        gender,
        confidence: confidence * 100,
        emotion: majorEmotion,
        success: true
      })
    }).catch(function (err) {
      console.log(err, 'face api error>>>>>>>>>>>>', err)
      return callback({ err, success: false })
    })
  }
  if (type == 'T') {
    console.log('quick feedback---------------------->Text')
    let detectedLanguage = await languageIndexer(payload.textData);
    try {
      let convertedText = await languageConverter(
        payload.textData.documents,
        languageMapping[detectedLanguage?detectedLanguage:"en-US"]
      )
      // console.log('converted text--------------------->', convertedText)
      let textResponse = await textIndexer({
        documents: convertedText
      })
      let detectedMajorSentimentText = null
      let detectedMajorSentimentTextScore = 0
      let AllSentiments1 = {
        neutral: 0,
        positive: 0,
        negative: 0
      }

      console.log("text response--------------------->",textResponse);
      if(textResponse.documents){
       textResponse.documents.forEach(document => {
        AllSentiments1[document.sentiment=="mixed"?"neutral":document.sentiment] += document.confidenceScores[document.sentiment=="mixed"?"neutral":document.sentiment]
       })
      }

      console.log(AllSentiments1, 'ALL Senitments1-------------->')
      Object.keys(AllSentiments1).map(o => {
        if (AllSentiments1[o] > detectedMajorSentimentTextScore) {
          detectedMajorSentimentTextScore = AllSentiments1[o]
          detectedMajorSentimentText = o
        }
      })
      return callback({
        detectedMajorSentimentText,
        detectedMajorSentimentTextScore,
        interactionId,
        success: true
      })
    } catch (err) {
      console.log(err, 'ANALYZING TEXT error>>>>>>>>>>>>', err)
      return callback({ err, success: false })
    }
  }
}

exports.quickFeedback = quickFeedback
