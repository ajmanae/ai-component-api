'use strict'
const pg = require('../../../../core/api/connectors/postgress');
const rp = require('request-promise');


module.exports= function indexService(indexId,audioId) {
   setTimeout(async()=>{
   try {
    let accountId = '18c0e61d-d611-4526-86f7-ffe0f75fe9d5';
    let location = 'eastus';
    let key = '72c126cececc496a909566afb5f36441';
    let uri= 'https://api.videoindexer.ai';
    let getTokenUri=`${uri}/Auth/${location}/Accounts?generateAccessTokens=true&allowEdit=true`;

    var getTokenOptions = {
        method: 'GET',
        uri: getTokenUri,
        headers: {
           'Ocp-Apim-Subscription-Key': key
        },
        json:true
    };
    
    //get access token
    let tokenResponse = await rp(getTokenOptions);
    console.log(tokenResponse,"Token Response------------------------------->");

    let accessToken = tokenResponse[tokenResponse.length-1].accessToken;

    //indexing audio

    let IndexUri = `https://api.videoindexer.ai/${location}/Accounts/${accountId}/Videos/${audioId}/Index?accessToken=${accessToken}`;

    console.log(IndexUri,"Index URI------------------------------------>")

    var IndexOptions = {
        method: 'GET',
        uri: IndexUri,
        headers: {
           'Ocp-Apim-Subscription-Key': key,
        },
        json:true
    };

    let IndexResponse = await rp(IndexOptions);

   //  console.log("Index Response------------>",IndexResponse);

    if(IndexResponse.state=="Processed"){
       let detailResponse ={
         tranxData:JSON.stringify(IndexResponse),
         sentiments:JSON.stringify(IndexResponse.summarizedInsights.sentiments),
         emotions:JSON.stringify(IndexResponse.summarizedInsights.emotions),
         majorSentiment:"",
         majorEmotion:""
       };
       let AllSentiments={
          'Neutral':0,
          'Positive':0,
          'Negative':0
       }

       let AllEmotions={
         'Anger':0,
         'Joy':0,
         'Fear':0,
         'Sad':0
      }

       IndexResponse.summarizedInsights.sentiments.forEach(sentiment => {
          sentiment.appearances.forEach(appearance => {
             AllSentiments[sentiment.sentimentKey] += appearance.endSeconds - appearance.startSeconds
          });
       });

       console.log(AllSentiments,"ALL Senitments-------------->")

       let maxValue=0;

       Object.keys(AllSentiments).map(o=>{
          if(AllSentiments[o]>maxValue){
              maxValue = AllSentiments[o];
            detailResponse.majorSentiment=o;
          }
       })

       console.log(detailResponse.majorSentiment,"Major Sentiment--------------------------->")

       IndexResponse.summarizedInsights.emotions.forEach(emotion => {
         emotion.appearances.forEach(appearance => {
            AllEmotions[emotion.type] += appearance.endSeconds - appearance.startSeconds
         });
      });

      console.log(AllEmotions,"ALL Emotions-------------->")

      let maxValue1=0;

      Object.keys(AllEmotions).map(o=>{
         if(AllEmotions[o]>maxValue1){
           detailResponse.majorEmotion=o;
         }
      })

      console.log(detailResponse.majorEmotion,"Major Emotion--------------------------->")
      
      let connection = await pg.connection();
      let query1 = `UPDATE smiles.public.audiodetails SET tranxdata='${detailResponse.tranxData}',emotions='${detailResponse.emotions}',sentiments='${detailResponse.sentiments}',majorsentiment='${detailResponse.majorSentiment}',majoremotion='${detailResponse.majorEmotion}' WHERE indexid='${indexId}'`;

      console.log("query", query1);
      let responseData1 = await connection.query(query1);
      console.log(responseData1,"response data------------------------------>")
    }
    else{
       indexService(indexId,audioId)
    }

   

     
   } catch (err) {
       console.log("audio index service error ---------------------------------->",err);
        indexService(indexId,audioId)
   }
},30000)
}
// = indexService;