'use strict'
const pg = require('../../../../core/api/connectors/postgress');
const rp = require('request-promise');

async function upload(payload, UUIDKey, route, callback, JWToken) {
   console.log("payload",payload);
   try {
    let accountId = '18c0e61d-d611-4526-86f7-ffe0f75fe9d5';
    let location = 'eastus';
    let key = '72c126cececc496a909566afb5f36441';
    let uri= 'https://api.videoindexer.ai';
    let getTokenUri=`${uri}/Auth/${location}/Accounts?generateAccessTokens=true&allowEdit=true`;

    

    var getTokenOptions = {
        method: 'GET',
        uri: getTokenUri,
        headers: {
           'Ocp-Apim-Subscription-Key': key
        },
        json:true
    };
    
    //get access token
    let tokenResponse = await rp(getTokenOptions);
    console.log(tokenResponse,"Token Response------------------------------->");

    let accessToken = tokenResponse[tokenResponse.length-1].accessToken;

    //uplaoding audio

    let audioUploadUri = `${uri}/${location}/Accounts/${accountId}/Videos?name=${payload.name}&externalId=${payload.name}&accessToken=${accessToken}`;

    console.log(audioUploadUri,"audio URI------------------------------------>")

    let file = payload.files.file;
    // const basePath = String(config.get('basePath'));
    // const completeBasePath = path.normalize(path.join(basePath));
    // !fs.existsSync(completeBasePath) ? fs.mkdirSync(completeBasePath, {
    //   mode: 777,
    //   recursive: true
    // }) : null;
    // const completePath = path.normalize(path.join(completeBasePath, file.name));
    // file.mv(completePath);
    // console.log(completePath,"complete Path------------------->")

    // var bodyFormData = new FormData();
    // bodyFormData.append('file', file);
    // let audioResponse = await axios({
    //   method: 'post',
    //   url:audioUploadUri,
    //   data:bodyFormData,
    //   headers:{
    //     'Content-Type': 'multipart/form-data',
    //     'Ocp-Apim-Subscription-Key': key,
    //   }
    // });


    var audioOptions = {
        method: 'POST',
        uri: audioUploadUri,
        formData:{
            file:{
                value:file.data,
                options:{
                    filename:file.name,
                    contentType:file.mimetype
                }
            }
        },
        headers: {
           'Content-Type': 'multipart/form-data',
           'Ocp-Apim-Subscription-Key': key,
        },
        json:true
    };

    let audioResponse = await rp(audioOptions);

    console.log("audio Response------------>",audioResponse);

    // callback(audioResponse)

      let query = `INSERT INTO smiles.public.audiouploads (id , externalid, state, tranxdata) 
        VALUES ('${audioResponse.id}', '${audioResponse.externalId}', '${audioResponse.state}', '${JSON.stringify(audioResponse)}')`
      console.log("query", query);
      pg.connection().then((conn) => {
         return Promise.all([
            conn.query(query)
         ]).then((data) => {
            console.log(data)
            callback({
               ...data,
               success:true
            });
         });
      });
   } catch (err) {
      return callback({...err,success:false});
   }
}
exports.upload = upload;