'use strict'
const pg = require('../../../../core/api/connectors/postgress');
const rp = require('request-promise');
const indexService = require('./indexService');


async function videoIndex(payload, UUIDKey, route, callback, JWToken) {
   console.log("payload",payload);
   try {

      let query = `SELECT * FROM smiles.public.videouploads WHERE externalid='${payload.id}'`
      console.log("query", query);
      let connection = await pg.connection();
      let responseData = await connection.query(query);
      // console.log(responseData.rows,"responseData------------------------>");
      let videoId;
      if(responseData.rows.length){
         videoId=responseData.rows[0].id
      }
      else{
         callback({
            status:404
         })
      }
      
       let detailResponse ={
         majorSentiment:"",
         state:"Processed",
         majorEmotion:"",
         userEmotion:null,
         externalId:payload.id,
         videoId:videoId,
         indexId:`${new Date().getTime()}`,
       };
      

      let query1 = `INSERT INTO smiles.public.videodetails (indexid, videoid, externalid, majorsentiment, majoremotion) 
      VALUES ('${detailResponse.indexId}', '${detailResponse.videoId}', '${detailResponse.externalId}','${detailResponse.majorSentiment}','${detailResponse.majorEmotion}')`;
      console.log("query", query1);
      let responseData1 = await connection.query(query1);

      indexService(detailResponse.indexId,detailResponse.videoId);

      callback({...detailResponse,success:true});
     
   } catch (err) {
      return callback({...err,success:false});
   }
}
exports.videoIndex = videoIndex;