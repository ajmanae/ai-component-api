'use strict'
const pg = require('../../../../core/api/connectors/postgress');
const rp = require('request-promise');

async function upload(payload, UUIDKey, route, callback, JWToken) {
   console.log("payload",payload);
   try {
    let accountId = '18c0e61d-d611-4526-86f7-ffe0f75fe9d5';
    let location = 'eastus';
    let key = '72c126cececc496a909566afb5f36441';
    let uri= 'https://api.videoindexer.ai';
    let getTokenUri=`${uri}/Auth/${location}/Accounts?generateAccessTokens=true&allowEdit=true`;

    

    var getTokenOptions = {
        method: 'GET',
        uri: getTokenUri,
        headers: {
           'Ocp-Apim-Subscription-Key': key
        },
        json:true
    };
    
    //get access token
    let tokenResponse = await rp(getTokenOptions);
    console.log(tokenResponse,"Token Response------------------------------->");

    let accessToken = tokenResponse[tokenResponse.length-1].accessToken;

    //uplaoding video

    let videoUploadUri = `${uri}/${location}/Accounts/${accountId}/Videos?name=${payload.name}&externalId=${payload.name}&accessToken=${accessToken}`;

    console.log(videoUploadUri,"Video URI------------------------------------>")

    let file = payload.files.file;
    // const basePath = String(config.get('basePath'));
    // const completeBasePath = path.normalize(path.join(basePath));
    // !fs.existsSync(completeBasePath) ? fs.mkdirSync(completeBasePath, {
    //   mode: 777,
    //   recursive: true
    // }) : null;
    // const completePath = path.normalize(path.join(completeBasePath, file.name));
    // file.mv(completePath);
    // console.log(completePath,"complete Path------------------->")

    // var bodyFormData = new FormData();
    // bodyFormData.append('file', file);
    // let videoResponse = await axios({
    //   method: 'post',
    //   url:videoUploadUri,
    //   data:bodyFormData,
    //   headers:{
    //     'Content-Type': 'multipart/form-data',
    //     'Ocp-Apim-Subscription-Key': key,
    //   }
    // });


    var videoOptions = {
        method: 'POST',
        uri: videoUploadUri,
        formData:{
            file:{
                value:file.data,
                options:{
                    filename:file.name,
                    contentType:file.mimetype
                }
            }
        },
        headers: {
           'Content-Type': 'multipart/form-data',
           'Ocp-Apim-Subscription-Key': key,
        },
        json:true
    };

    let videoResponse = await rp(videoOptions);

    console.log("video Response------------>",videoResponse);

    // callback(videoResponse)

      let query = `INSERT INTO smiles.public.videouploads (id , externalid, state, tranxdata) 
        VALUES ('${videoResponse.id}', '${videoResponse.externalId}', '${videoResponse.state}', '${JSON.stringify(videoResponse)}')`
      console.log("query", query);
      pg.connection().then((conn) => {
         return Promise.all([
            conn.query(query)
         ]).then((data) => {
            console.log(data)
            callback({
               ...data,
               success:true
            });
         });
      });
   } catch (err) {
      return callback({...err,success:false});
   }
}
exports.upload = upload;